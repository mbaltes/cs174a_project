package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ReplenishmentItem {
	@Column(name="itemId")
	private int itemId;
	@Column(name="orderId")
	private int orderId;
	@Column(name="manufacturer")
	private String manufacturer;
	@Column(name="model")
	private String model;
	@Column(name="quantity")
	private int quantity;
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}