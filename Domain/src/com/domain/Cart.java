package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class Cart {
	@Column(name="cid")
	private Integer cid;
	@Column(name="customerId")
	private String customerId;
	@Column(name="stockNumber")
	private String stockNumber;
	@Column(name="quantity")
	private Integer quantity;
	
	//Transient value
	@Column(name="price", nullable=true)
	private Float price;
	
	public void setCart(int cid, String customerId, String stockNumber, int quantity) {
		this.cid = cid;
		this.customerId = customerId;
		this.stockNumber = stockNumber;
		this.quantity = quantity;
	}
	
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getStockNumber() {
		return stockNumber;
	}
	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return cid + ",'" + customerId + "','" + stockNumber + "'," + quantity;
	}
	public static void printHeader() {
		System.out.printf("%-15s %-15s %-15s %-15s\n", "CID", "Customer ID", "Stock Number", "Quantity");
		System.out.println("---------------------------------------------------------");
	}	
	
	public void printRowDisplay() {
		System.out.printf("%-15s %-15s %-15s %-15s", cid.toString(), customerId, stockNumber, quantity.toString());  
	}
	
}