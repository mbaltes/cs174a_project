package com.domain;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Catalog {

	@Column(name="stockNumber")
	private String stockNumber;
	@Column(name="category")
	private String category;
	@Column(name="manufacturer")
	private String manufacturer;
	@Column(name="model")
	private String model;
	@Column(name="warranty")
	private Integer warranty;
	@Column(name="price")
	private Float price;
	
	public List<Description> descriptions;
	
	public String getStockNumber() {
		return stockNumber;
	}
	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getWarranty() {
		return warranty;
	}
	public void setWarranty(int warranty) {
		this.warranty = warranty;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "'"+stockNumber + "','" + category
				+ "','" + manufacturer + "','" + model
				+ "'," + warranty + "," + price;
	}
	
	public static void printHeader() {
		System.out.printf("%-15s %-15s %-15s %-15s %-15s %-15s\n", "Stock Number", "Category", "Manufacturer", "Model", "Warranty", "Price");
		System.out.println("---------------------------------------------------------------------------------------");
	}	
	
	public void printRowDisplay() {
		System.out.printf("%-15s %-15s %-15s %-15s %-15s %-15s\n", stockNumber, category, manufacturer, model, warranty,price.toString());
	}
}