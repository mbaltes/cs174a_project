package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ShippingNotice {
	@Column(name="noticeId")
	private int noticeId;
	@Column(name="shippingCompany")
	private String shippingCompany;
	@Column(name="time")
	private String time;
	@Column(name="received")
	private int received;
	public int getNoticeId() {
		return noticeId;
	}
	public void setNoticeId(int noticeId) {
		this.noticeId = noticeId;
	}
	public String getShippingCompany() {
		return shippingCompany;
	}
	public void setShippingCompany(String shippingCompany) {
		this.shippingCompany = shippingCompany;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getReceived() {
		return received;
	}
	public void setReceived(int received) {
		this.received = received;
	}
	@Override
	public String toString() {
		return noticeId + ","
				+ shippingCompany + "," + time + "," + received;
	}
}