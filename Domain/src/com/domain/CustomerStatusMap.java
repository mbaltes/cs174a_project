package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class CustomerStatusMap {
	@Column(name="mapId")
	private int mapId;
	@Column(name="customerId")
	private String customerId;
	@Column(name="statusId")
	private int statusId;
	public int getMapId() {
		return mapId;
	}
	public void setMapId(int mapId) {
		this.mapId = mapId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	@Override
	public String toString() {
		return mapId + ",'"+ customerId + "'," + statusId;
	}
}