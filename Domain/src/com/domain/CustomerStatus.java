package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class CustomerStatus {
	@Column(name="statusId")
	private int statusId;
	@Column(name="name")
	private String name;
	@Column(name="discount")
	private float discount;
	@Column(name="shipRate")
	private float shipRate;
	@Column(name="freeShipping")
	private float freeShipping;
	@Column(name="minimum")
	private float minimum;
	
	public float getMinimum() {
		return minimum;
	}
	public void setMinimum(float minimum) {
		this.minimum = minimum;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getDiscount() {
		return discount;
	}
	public void setDiscount(float discount) {
		this.discount = discount;
	}
	public float getShipRate() {
		return shipRate;
	}
	public void setShipRate(float shipRate) {
		this.shipRate = shipRate;
	}
	public float getFreeShipping() {
		return freeShipping;
	}
	public void setFreeShipping(float freeShipping) {
		this.freeShipping = freeShipping;
	}
	@Override
	public String toString() {
		return statusId + ",'" + name
				+ "'," + discount + "," + shipRate
				+ "," + freeShipping + ","+minimum;
	}
	
	public static void printHeader() {
		System.out.printf("%-15s %-15s %-15s %-15s %-15s %-15s\n", "Status Id", "Name", "Discount", "Ship Rate", "Req. For FS", "Minimum");
		System.out.println("-------------------------------------------------------------------------------------------------");
	}	
	
	public void printRowDisplay() {
		System.out.printf("%-15s %-15s %-15s %-15s %-15s %-15s\n", statusId, name, discount, shipRate, freeShipping,minimum);
	}
	
}