package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ReplenishmentOrder {
	@Column(name="orderId")
	private int orderId;
	@Column(name="manufacturer")
	private String manufacturer;
	@Column(name="time")
	private String time;
	@Column(name="received")
	private int received;
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getReceived() {
		return received;
	}
	public void setReceived(int received) {
		this.received = received;
	}
	@Override
	public String toString() {
		return orderId + ","
				+ manufacturer + "," + time + "," + received;
	}
}
