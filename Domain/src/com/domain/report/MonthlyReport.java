package com.domain.report;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.domain.Customer;

public class MonthlyReport {
	private Customer highestPayingCustomer;
	private Map<String, List<MonthlyReportItem>> categoryToReportMap = new HashMap<String, List<MonthlyReportItem>>();

	public Customer getHighestPayingCustomer() {
		return highestPayingCustomer;
	}

	public void setHighestPayingCustomer(Customer highestPayingCustomer) {
		this.highestPayingCustomer = highestPayingCustomer;
	}

	public Map<String, List<MonthlyReportItem>> getCategoryToReportMap() {
		return categoryToReportMap;
	}

	public void setCategoryToReportMap(
			Map<String, List<MonthlyReportItem>> categoryToReportMap) {
		this.categoryToReportMap = categoryToReportMap;
	}
	
	public void printRowDisplay() {
		for (Entry entry : categoryToReportMap.entrySet())
		{
			System.out.println("/////////////////////////");
			System.out.println("//Items sold for category: " + (String) entry.getKey());
			System.out.println("/////////////////////////");
			List<MonthlyReportItem> listOfItems = (List<MonthlyReportItem>) entry.getValue();
			if (listOfItems != null && listOfItems.size() > 0)
			{
				MonthlyReportItem.printHeader();
				for (MonthlyReportItem monthlyReportItem : listOfItems)
				{
					monthlyReportItem.printRowDisplay();
				}
			}
			else {
				System.out.println("No items sold in this category.\n");
			}
		}
		
		System.out.println();
		if (highestPayingCustomer == null || highestPayingCustomer.getCustomerId() == null) {
			System.out.println("Highest paying customer: There is no highest paying customer.");
		} else {
			System.out.println("Highest paying customer: " + highestPayingCustomer.getCustomerId());
		}
	}
}
