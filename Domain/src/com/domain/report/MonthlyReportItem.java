package com.domain.report;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class MonthlyReportItem {
	//Print monthly summary of sales: the amount (quantity, price) of sale per product, per category, and the
	//customer who did the most purchase.
	
	@Column(name="stockNumber")
	String stockNumber;
	@Column(name="model")
	String model;
	@Column(name="totalQuantity")
	int quantity;
	@Column(name="totalPrice")
	float price;
	@Column(name="category")
	String category;
	
	public String getStockNumber() {
		return stockNumber;
	}
	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public static void printHeader() {
		System.out.printf("%-15s %-15s %-15s %-15s\n", "Stock Number", "Model", "Quantity Sold", "Total");;
		System.out.println("----------------------------------------------------------------------------");
	}	
	
	public void printRowDisplay() {
		System.out.printf("%-15s %-15s %-15s $%-15s\n", stockNumber, model, quantity, price);
	}
}
