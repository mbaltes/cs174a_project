package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Accessory {

	@Column(name="aid")
	private int aid;
	@Column(name="parent")
	private String parent;
	@Column(name="child")
	private String child;
	public int getAid() {
		return aid;
	}
	public void setAid(int aid) {
		this.aid = aid;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getChild() {
		return child;
	}
	public void setChild(String child) {
		this.child = child;
	}
	@Override
	public String toString() {
		return aid + ",'" + parent + "','" + child + "'";
	}
	
}