package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Description {

	@Column(name="did")
	private int did;
	@Column(name="stockNumber")
	private String stockNumber;
	@Column(name="attribute")
	private String attribute;
	@Column(name="value")
	private String value;
	public int getDid() {
		return did;
	}
	public void setDid(int did) {
		this.did = did;
	}
	public String getStockNumber() {
		return stockNumber;
	}
	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return did + ",'" + stockNumber + "','" + attribute + "','" + value +"'";
	}
	
	public static void printHeader() {
		System.out.printf("\n%-15s %-15s\n","","Specifications");
		System.out.printf("%-15s %-15s\n", "", "--------------------------------");
	}
	
	public void printRowDisplay() {
		System.out.printf("%-15s %-15s %-15s\n","", getAttribute()+":", getValue());
	}
}