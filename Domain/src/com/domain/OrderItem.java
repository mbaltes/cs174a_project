package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class OrderItem {
	@Column(name="id")
	private int id;
	@Column(name="orderId")
	private int orderId;
	@Column(name="stockNumber")
	private String stockNumber;
	@Column(name="quantity")
	private int quantity;
	
	public void setOrderItem(int id, int orderId, String stockNumber, int quantity) {
		this.id = id;
		this.orderId = orderId;
		this.stockNumber = stockNumber;
		this.quantity = quantity;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getStockNumber() {
		return stockNumber;
	}
	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return id + "," + orderId
				+ ",'" + stockNumber + "'," + quantity;
	}
	public static void printHeader() {
		System.out.printf("%-15s %-15s %-15s %-15s\n", "ID", "Order ID", "Stock Number", "Quantity");
		System.out.println("-----------------------------------------------------------------------------------------------------------------");
	}	
	
	public void printRowDisplay() {
		System.out.printf("%-15s %-15s %-15s %-15s\n", id, orderId, stockNumber, quantity);
	}
}