package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ShippingItem {
	@Column(name="itemId")
	private int itemId;
	@Column(name="noticeId")
	private int noticeId;
	@Column(name="manufacturer")
	private String manufacturer;
	@Column(name="model")
	private String model;
	@Column(name="quantity")
	private int quantity;
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getNoticeId() {
		return noticeId;
	}
	public void setNoticeId(int noticeId) {
		this.noticeId = noticeId;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return itemId + "," + noticeId
				+ "," + manufacturer + "," + model
				+ "," + quantity;
	}
	
}