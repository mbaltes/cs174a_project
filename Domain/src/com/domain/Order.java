package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Order {
	@Column(name="orderId")
	private int orderId;
	@Column(name="customerId")
	private String customerId;
	@Column(name="statusId")
	private int statusId;
	@Column(name="subtotal")
	private float subtotal;
	@Column(name="total")
	private float total;
	@Column(name="time")
	private String time;
	@Column(name="processed")
	private int processed;
	
	public void setOrder(int orderId, String customerId, int statusId, float subtotal,
			float total, String time, int processed) {
		this.orderId = orderId;
		this.customerId = customerId;
		this.statusId = statusId;
		this.subtotal = subtotal;
		this.total = total;
		this.time = time;
		this.processed = processed;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	public float getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(float subtotal) {
		this.subtotal = subtotal;
	}
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getProcessed() {
		return processed;
	}
	public void setProcessed(int processed) {
		this.processed = processed;
	}
	@Override
	public String toString() {
		return orderId + ",'" + customerId
				+ "','" + statusId + "'," + subtotal
				+ "," + total + ",'" + time + "',"
				+ processed;
	}
	
	public static void printHeader() {
		System.out.printf("%-15s %-15s %-15s %-15s %-15s %-15s %-15s\n", "Order ID", "Customer ID", "Status ID", "Subtotal", "Total", "Time", "Processed");
		System.out.println("-----------------------------------------------------------------------------------------------------------------");
	}	
	
	public void printRowDisplay() {
		System.out.printf("%-15s %-15s %-15s $%-15s $%-15s %-15s %-15s\n", orderId, customerId, statusId, subtotal, total, time, processed);
	}
}