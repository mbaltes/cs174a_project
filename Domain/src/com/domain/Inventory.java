package com.domain;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Inventory {
	@Column(name="stockNumber")
	private String stockNumber;
	@Column(name="manufacturer")
	private String manufacturer;
	@Column(name="model")
	private String model;
	@Column(name="quantity")
	private int quantity;
	@Column(name="minimum")
	private int min;
	@Column(name="maximum")
	private int max;
	@Column(name="location")
	private String location;
	@Column(name="replenishment")
	private int replenishment;
	public String getStockNumber() {
		return stockNumber;
	}
	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getMin() {
		return min;
	}
	public void setMin(int min) {
		this.min = min;
	}
	public int getMax() {
		return max;
	}
	public void setMax(int max) {
		this.max = max;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getReplenishment() {
		return replenishment;
	}
	public void setReplenishment(int replenishment) {
		this.replenishment = replenishment;
	}
	@Override
	public String toString() {
		return "'" +stockNumber + "','"
				+ manufacturer + "','" + model + "'," + quantity
				+ "," + min + "," + max + ",'" + location
				+ "'," + replenishment;
	}
	
	public static void printHeader() {
		System.out.printf("%-15s %-15s %-15s %-15s %-15s %-15s %-15s %-15s\n", "Stock Number", "Manufacturer", "Model", "Quantity", "Minimum", "Maximum", "Location", "Replenishment");
		System.out.println("--------------------------------------------------------------------------------------------------------");
	}
	
	public void printRowDisplay() {
		System.out.printf("%-15s %-15s %-15s %-15s %-15s %-15s %-15s %-15s\n", 
				          getStockNumber(), getManufacturer(), getModel(), getQuantity(), getMin(), getMax(), getLocation(), getReplenishment());
	}
	
}