package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class BaseDAO {
	
	protected static Connection connection;
	private static Boolean isInitialized = Boolean.FALSE; 
	
	public BaseDAO() {
		//Initialize the database
		if (!isInitialized)
		{
			initializeConnection();
		}
	}
	
	public <T> List<T> query(Class clss, String query, Boolean resultExpected) throws SQLException
	{
		if (isInitialized)
		{
			if (connection.isClosed())
			{
				//if the connection is closed, reinitialize the connection
				initializeConnection();
			}
		}

		ResultSetMapper<T> resultSetMapper = new ResultSetMapper<T>();
		ResultSet resultSet = null;
		
		PreparedStatement statement = connection.prepareStatement(query);
		resultSet = statement.executeQuery();
		
		//Prepare and execute the statement
		List<T> list = null;
		if (resultExpected)
		{
		
			//Map the result to the object
			list = resultSetMapper.mapResultSetToObject(resultSet, clss);
		}
		
		//Free the resources used by result set
		resultSet.close();
		
		//Return the list
		return list;
	}
	
	public ResultSet queryWithoutMap(String query) throws SQLException
	{
		if (isInitialized)
		{
			if (connection.isClosed())
			{
				//if the connection is closed, reinitialize the connection
				initializeConnection();
			}
		}
		
		ResultSet resultSet = null;
		
		PreparedStatement statement = connection.prepareStatement(query);
		resultSet = statement.executeQuery();

		//Return the list
		return resultSet;
	}
	
	public void initializeConnection() {
		// 1. Load the Oracle JDBC driver for this program
		try {
			DriverManager.registerDriver(new oracle.jdbc.OracleDriver());

			// Connect to the database
			String url = "jdbc:oracle:thin:@uml.cs.ucsb.edu:1521:xe";
			String username = "tylercoffman";
			String password = "7074354";
			
			//Get the connection
			connection = DriverManager.getConnection(url, username, password);
			isInitialized = Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static Connection getConnection() {
		return connection;
	}
}
