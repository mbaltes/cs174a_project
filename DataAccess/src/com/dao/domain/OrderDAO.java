package com.dao.domain;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.dao.BaseDAO;
import com.domain.Customer;
import com.domain.Order;

public class OrderDAO extends BaseDAO {
	
	CustomerDAO customerDAO = new CustomerDAO();
	
	public void insert(Order order) {
		if (order.getOrderId() == 0)
		{
			Order maxOrder = findMaxOrderId();
			int maxOrderId;
			//It is only null for the first entry
			if (maxOrder == null)
				maxOrderId = 1;
			else
				maxOrderId = maxOrder.getOrderId() + 1;
			order.setOrderId(maxOrderId);
		}
		String query = "INSERT INTO Orders VALUES (" + order.getOrderId()
				+ ",'" + order.getCustomerId() + "'," + order.getStatusId()
				+ "," + order.getSubtotal() + "," + order.getTotal() + ",'"
				+ order.getTime() + "'," + order.getProcessed() + ")";
		try {
			query(Order.class, query, false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Order> findByCustomer(Customer customer) {
		String query = "SELECT * FROM Orders O WHERE O.customerId='" + customer.getCustomerId() + "' ORDER BY O.orderID ASC";
		try {
			List<Order> orders = query(Order.class,query,true);
			if (orders == null)
				return new ArrayList<Order>();
			else
				return orders;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ArrayList<Order>();
	}
	
	public List<Order> findLastThreeByCustomer(Customer customer)
	{
		String query = "SELECT * FROM Orders O WHERE O.customerId='" + customer.getCustomerId() + "' AND ROWNUM <= 3 ORDER BY O.orderId DESC ";
		try {
			List<Order> orders = query(Order.class,query,true);
			if (orders == null)
				return new ArrayList<Order>();
			else
				return orders;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ArrayList<Order>();
	}
	
	public List<Order> findAll()
	{
		String query = "SELECT * FROM Orders O ORDER BY O.orderId ASC";
		
		try {
			List<Order> orders = query(Order.class,query,true);
			if (orders == null) 
				return new ArrayList<Order>();
			else
				return orders;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ArrayList<Order>();
	}
	
	public Customer findHighestSpendingCustomer(String startDate, String endDate) {
		
		String query = "SELECT C1.customerId, SUM(O1.total) as Total FROM Customers C1, Orders O1 WHERE O1.time >= '" + startDate 
				+ "' AND O1.time <= '" + endDate + "' AND C1.customerId = O1.customerId GROUP BY C1.customerId ORDER BY Total DESC";
		
		try {
			List<Customer> customers = query(Customer.class, query, true);
			
			if (customers == null || customers.size() == 0)
			{
				return null;
			}
			else
				return customers.get(0);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Order findMaxOrderId()
	{
		String query = "SELECT NVL(MAX(O.orderId),0) as orderId FROM Orders O";
		try {
			List<Order> orders = query(Order.class,query,true);
			if (orders != null && orders.size() > 0)
			{
				return orders.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Order findByOrderId(int orderId)
	{
		String query = "SELECT * FROM Orders O WHERE O.orderId ="+orderId;
		try {
			List<Order> orders = query(Order.class,query,true);
			if (orders != null && orders.size() > 0)
			{
				return orders.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public List<Order> findAllUnprocessed() {
		String query = "SELECT * FROM Orders WHERE processed = 0";
		try {
			List<Order> orders = query(Order.class,query,true);
			if (orders != null && orders.size() > 0)
			{
				return orders;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void deleteUnnecessarySalesTransactions() {
		
		List<Customer> customers = customerDAO.findAll();
		
		for (Customer customer : customers) {
			String selectUnnecessaryOrdersQuery = "SELECT O.orderId FROM Orders O WHERE O.customerId = '" + customer.getCustomerId() + "' ORDER BY O.orderId DESC";
			
			try {
				List<Order> orders = query(Order.class, selectUnnecessaryOrdersQuery, true);
				
				if (orders != null) {
					if (orders.size() > 3)
					{
						//Delete 3. Im so sorry I did this. :(
						orders.remove(0);
						orders.remove(0);
						orders.remove(0);
						
						if (orders != null && orders.size() > 0) {
							deleteOrderItems(orders);
							deleteOrders(orders);
						}
					}
				}
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void deleteOrders(List<Order> orders) {
		String deleteOrdersQuery = "DELETE FROM Orders O WHERE O.orderId IN (";
		for (int i = 0; i < orders.size(); i++) {
			deleteOrdersQuery += orders.get(i).getOrderId();
			if (i + 1 != orders.size())
				deleteOrdersQuery += ",";
		}
		deleteOrdersQuery += ")";
		
		try {
			query(Order.class, deleteOrdersQuery, false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteOrderItems(List<Order> orders) {
		String deleteOrderItemsQuery = "DELETE FROM OrderItems OI WHERE OI.orderId IN (";
		for (int i = 0; i < orders.size(); i++) {
			deleteOrderItemsQuery += orders.get(i).getOrderId();
			if (i + 1 != orders.size())
				deleteOrderItemsQuery += ",";
		}
		deleteOrderItemsQuery += ")";
		
		try {
			query(Order.class, deleteOrderItemsQuery, false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void markOrderProcessed(Order order) {
		String query = "UPDATE Orders SET processed = 1 WHERE orderId = " + order.getOrderId();
		try {
			query(Order.class, query, false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}