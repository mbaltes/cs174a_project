package com.dao.domain;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dao.BaseDAO;
import com.domain.Customer;

public class CustomerDAO extends BaseDAO {
	public List<Customer> findAll() {
		String query = "SELECT * FROM Customers";
		try {
			List<Customer> customers = query(Customer.class, query, true);

			if (customers == null || customers.size() == 0) {
				return new ArrayList<Customer>();
			} else {
				return customers;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<Customer>();
	}
	public Customer findByCustomerId(String customerId)
	{
		String query = "SELECT * FROM Customers WHERE customerId = '" + customerId + "'";
		List<Customer> customers;
		try {
			customers = query(Customer.class, query, true);
			if (customers == null || customers.size() == 0) {
				return null;
			} else {
				return customers.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
