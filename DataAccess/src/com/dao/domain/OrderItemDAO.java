package com.dao.domain;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;

import com.dao.BaseDAO;
import com.domain.Order;
import com.domain.OrderItem;

public class OrderItemDAO extends BaseDAO {
	public List<OrderItem> findAllByOrder(Order order) {
		String query = "SELECT * FROM OrderItems O WHERE O.orderId="
				+ order.getOrderId();

		try {
			List<OrderItem> orderItems = query(OrderItem.class, query, true);
			if (orderItems == null)
				return new ArrayList<OrderItem>();
			else
				return orderItems;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<OrderItem>();
	}
	
	public void insert(OrderItem orderItem) {
		if (orderItem.getId() == 0)
		{
			OrderItem maxOrder = findMaxOrderItemId();
			int maxOrderItemId;
			//It is only null for the first entry
			if (maxOrder == null)
				maxOrderItemId = 1;
			else
				maxOrderItemId = maxOrder.getId() + 1;
			orderItem.setId(maxOrderItemId);
		}
		
		String query = "INSERT INTO OrderItems VALUES (" + orderItem.getId() + "," + orderItem.getOrderId() + ",'" + orderItem.getStockNumber() + "'," + orderItem.getQuantity() + ")";
		try {
			query(Order.class, query, false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public OrderItem findMaxOrderItemId()
	{
		String query = "SELECT NVL(MAX(OI.id),0) as id FROM OrderItems OI";
		try {
			List<OrderItem> orderItems = query(OrderItem.class,query,true);
			if (orderItems != null && orderItems.size() > 0)
			{
				return orderItems.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
