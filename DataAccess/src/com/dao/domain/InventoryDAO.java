package com.dao.domain;

import java.sql.SQLException;
import java.util.List;

import com.dao.BaseDAO;
import com.domain.Inventory;
import com.domain.ReplenishmentItem;
import com.domain.ReplenishmentOrder;
import com.domain.ShippingItem;
import com.domain.ShippingNotice;

///Inventory, Shipping notices, and Replenishment DAO
public class InventoryDAO extends BaseDAO {

	public Inventory findByStockNumber(String stockNumber)
	{
		String query = "SELECT * FROM Inventory WHERE stockNumber = '" + stockNumber + "'";
	
		try {
			List<Inventory> inventoryItems = query(Inventory.class, query, true);
			if (inventoryItems == null || inventoryItems.size() == 0)
			{
				return null;
			}
			else
			{
				return inventoryItems.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Inventory findByLocation(String location)
	{
		String query = "SELECT * FROM Inventory WHERE location = '" + location + "'";
	
		try {
			List<Inventory> inventoryItems = query(Inventory.class, query, true);
			if (inventoryItems == null || inventoryItems.size() == 0)
			{
				return null;
			}
			else
			{
				return inventoryItems.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Inventory findByManufacturerModel(String manufacturer, String model) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM Inventory WHERE manufacturer = '");
		sb.append(manufacturer);
		sb.append("' AND model = '");
		sb.append(model);
		sb.append("'");
		String query = sb.toString();
		
		try {
			List<Inventory> inventoryItems = query(Inventory.class, query, true);
			if (inventoryItems == null || inventoryItems.size() == 0)
			{
				return null;
			}
			else
			{
				return inventoryItems.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Inventory> findDepletedByManufacturer(String manufacturer) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM Inventory WHERE quantity + replenishment < minimum AND manufacturer='");
		sb.append(manufacturer);
		sb.append("'");
		String query = sb.toString();
		try {
			List<Inventory> inventoryItems = query(Inventory.class, query, true);
			if (inventoryItems == null || inventoryItems.size() == 0)
			{
				return null;
			}
			else
			{
				return inventoryItems;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public ShippingNotice getShippingNoticeHighestPK() {
		String query = "SELECT * FROM ShippingNotices WHERE rownum = 1 ORDER BY noticeId DESC";
		
		try {
			List<ShippingNotice> items = query(ShippingNotice.class, query, true);
			if (items == null || items.size() == 0)
			{
				return null;
			}
			else
			{
				return items.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public ReplenishmentOrder getReplenishmentOrderHighestPK() {
		String query = "SELECT * FROM ReplenishmentOrders WHERE rownum = 1 ORDER BY orderId DESC";
		
		try {
			List<ReplenishmentOrder> items = query(ReplenishmentOrder.class, query, true);
			if (items == null || items.size() == 0)
			{
				return null;
			}
			else
			{
				return items.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public ShippingItem getShippingItemHighestPK() {
		String query = "SELECT * FROM ShippingItems WHERE rownum = 1 ORDER BY itemId DESC";
		//TODO: consider enforcing that manufacturer, model combinations be UNIQUE.
		try {
			List<ShippingItem> items = query(ShippingItem.class, query, true);
			if (items == null || items.size() == 0)
			{
				return null;
			}
			else
			{
				return items.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public ReplenishmentItem getReplenishmentItemHighestPK() {
		String query = "SELECT * FROM ReplenishmentItems WHERE rownum = 1 ORDER BY itemId DESC";
		//TODO: consider enforcing that manufacturer, model combinations be UNIQUE.
		try {
			List<ReplenishmentItem> items = query(ReplenishmentItem.class, query, true);
			if (items == null || items.size() == 0)
			{
				return null;
			}
			else
			{
				return items.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public ShippingNotice findShippingNoticeById(int noticeId) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM ShippingNotices WHERE noticeId = ");
		sb.append(noticeId);
		
		try {
			List<ShippingNotice> items = query(ShippingNotice.class, sb.toString(), true);
			if (items == null || items.size() == 0)
			{
				return null;
			}
			else
			{
				return items.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<ShippingNotice> findUnreceivedShippingNotices() {
		String query = "SELECT * FROM ShippingNotices WHERE received = 0";
		
		try {
			List<ShippingNotice> items = query(ShippingNotice.class, query, true);
			if (items == null || items.size() == 0)
			{
				return null;
			}
			else
			{
				return items;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<ShippingItem> findItemsByNoticeId(int noticeId) {
		String query =  "SELECT * FROM ShippingItems WHERE noticeId = " + noticeId;
		
			try {
				List<ShippingItem> items = query(ShippingItem.class, query, true);
				if (items == null || items.size() == 0)
				{
					return null;
				}
				else
				{
					return items;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		
	}
	
	//PERSISTENCE METHODS
	//----------------------------------------------------
	//
	public void persistInventory(Inventory inv) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO Inventory VALUES ('");
		sb.append(inv.getStockNumber());
		sb.append("', '");
		sb.append(inv.getManufacturer());
		sb.append("', '");
		sb.append(inv.getModel());
		sb.append("', ");
		sb.append(inv.getQuantity());
		sb.append(", ");
		sb.append(inv.getMin());
		sb.append(", ");
		sb.append(inv.getMax());
		sb.append(", '");
		sb.append(inv.getLocation());
		sb.append("', ");
		sb.append(inv.getReplenishment());
		sb.append(")");
		
		try {
			query(Inventory.class, sb.toString(), false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void persistShippingNotice(ShippingNotice notice) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ShippingNotices VALUES (");
		sb.append(notice.getNoticeId());
		sb.append(", '");
		sb.append(notice.getShippingCompany());
		sb.append("', '");
		sb.append(notice.getTime());
		sb.append("', ");
		sb.append(notice.getReceived());
		sb.append(")");
		           
		try {
			query(ShippingNotice.class, sb.toString(), false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void persistReplenishmentOrder(ReplenishmentOrder order) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ReplenishmentOrders VALUES (");
		sb.append(order.getOrderId());
		sb.append(", '");
		sb.append(order.getManufacturer());
		sb.append("', '");
		sb.append(order.getTime());
		sb.append("', ");
		sb.append(order.getReceived());
		sb.append(")");
		           
		try {
			query(ReplenishmentOrder.class, sb.toString(), false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void markShippingNoticeReceived(ShippingNotice notice) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ShippingNotices SET received = 1 WHERE noticeId = ");
		sb.append(notice.getNoticeId());
		try {
			query(ShippingNotice.class, sb.toString(), false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void persistShippingItem(ShippingItem item) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ShippingItems VALUES (");
		sb.append(item.getItemId());
		sb.append(", ");
		sb.append(item.getNoticeId());
		sb.append(", '");
		sb.append(item.getManufacturer());
		sb.append("', '");
		sb.append(item.getModel());
		sb.append("', ");
		sb.append(item.getQuantity());
		sb.append(")");
		String query = sb.toString();
		//System.out.println(query);
		try {
			query(ShippingItem.class, query, false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void persistReplenishmentItem(ReplenishmentItem item) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ReplenishmentItems VALUES (");
		sb.append(item.getItemId());
		sb.append(", ");
		sb.append(item.getOrderId());
		sb.append(", '");
		sb.append(item.getManufacturer());
		sb.append("', '");
		sb.append(item.getModel());
		sb.append("', ");
		sb.append(item.getQuantity());
		sb.append(")");
		String query = sb.toString();
		//System.out.println(query);
		try {
			query(ReplenishmentItem.class, query, false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateInventoryQuantityReplenishments(Inventory inv) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE Inventory SET quantity = ");
		sb.append(inv.getQuantity());
		sb.append(", replenishment = ");
		sb.append(inv.getReplenishment());
		sb.append(" WHERE stockNumber = '");
		sb.append(inv.getStockNumber());
		sb.append("'");
		String query = sb.toString();
		//System.out.println(query);
		try {
			query(Inventory.class, query, false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

