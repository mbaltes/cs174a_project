package com.dao.domain;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.dao.BaseDAO;
import com.domain.Catalog;
import com.domain.Description;

public class DescriptionsDAO extends BaseDAO {
	
	public List<Description> findByStockNumber(String stockNumber) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM Descriptions d WHERE d.stockNumber='");
		sb.append(stockNumber);
		sb.append("' ORDER BY d.attribute");
		List<Description> returnValue = null;
		try {
			returnValue = query(Description.class,sb.toString(), true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnValue = new LinkedList<Description>();
		}
		return returnValue;
	}
}
