package com.dao.domain;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dao.BaseDAO;
import com.domain.Cart;
import com.domain.Customer;

public class CartDAO extends BaseDAO {

	public void insert(Cart cart) {
		if (cart.getCid() == 0) {
			Cart maxCart = findMaxCartId();
			int maxCartId;
			// It is only null for the first entry
			if (maxCart == null)
				maxCartId = 1;
			else
				maxCartId = maxCart.getCid() + 1;
			cart.setCid(maxCartId);
		}
		String query = "INSERT INTO Cart VALUES (" + cart.getCid() + ",'"
				+ cart.getCustomerId() + "','" + cart.getStockNumber() + "',"
				+ cart.getQuantity() + ")";
		//System.out.println(query);

		try {
			query(Cart.class, query, false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void delete(String customerId, String stockNumber) {
		String query = "DELETE FROM Cart WHERE customerId='" + customerId
				+ "' AND stockNumber='" + stockNumber + "'";
		try {
			query(Cart.class, query, false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteAll(Customer customer) {
		String query = "DELETE FROM Cart WHERE customerId='"
				+ customer.getCustomerId() + "'";

		try {
			query(Cart.class, query, false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Cart findMaxCartId() {
		String query = "SELECT NVL(MAX(c.cid),0) as cid FROM Cart c";
		try {
			List<Cart> cartItems = query(Cart.class, query, true);
			if (cartItems != null && cartItems.size() > 0) {
				return cartItems.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public Cart findByStockNumber(String stockNumber) {
		String query = "SELECT * FROM Cart WHERE stockNumber='" + stockNumber + "'";
		
		try {
			List<Cart> cartItems = query(Cart.class,query, true);
			if (cartItems != null && cartItems.size() > 0) {
				return cartItems.get(0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Cart> findAll(Customer customer) {
		String query = "SELECT cart.*,cat.price FROM Cart cart, Catalog cat WHERE cart.customerId='"
				+ customer.getCustomerId()
				+ "' AND cart.stockNumber = cat.stockNumber";
		try {
			List<Cart> cartItems = query(Cart.class, query, true);
			if (cartItems == null)
				return new ArrayList<Cart>();
			else
				return cartItems;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ArrayList<Cart>();
	}
}