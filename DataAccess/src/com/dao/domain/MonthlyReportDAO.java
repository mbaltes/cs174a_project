package com.dao.domain;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.dao.BaseDAO;
import com.domain.report.MonthlyReportItem;

public class MonthlyReportDAO extends BaseDAO {
	/*
	 * Print monthly summary of sales: the amount (quantity, price) of sale per product, per category, and the
	 * customer who did the most purchase.
	 * 	@Column(name="stockNumber")
	String stockNumber;
	@Column(name="model")
	String model;
	@Column(name="totalQuantity")
	int quantity;
	@Column(name="totalPrice")
	float price;
	@Column(name="category")
	String category;
	 */
	public List<MonthlyReportItem> findMonthlyReportItemsByCategory(String category, String sDate, String eDate) {
		
		String query = "SELECT C.stockNumber, C.category, C.model, SUM(OI.quantity) as totalQuantity, SUM(O.total) as totalPrice "
				+ "FROM Catalog C, OrderItems OI, Orders O "
				+ "WHERE C.stockNumber = OI.stockNumber AND category = '" + category + "' AND OI.orderId = O.orderId AND O.time >= '" + sDate + "' AND O.time <= '" + eDate + "' "
				+ "GROUP BY C.stockNumber, C.category, C.model";
		
		
		try {
			List<MonthlyReportItem> monthlyReportItems = query(MonthlyReportItem.class, query, true);
			
			if (monthlyReportItems == null || monthlyReportItems.size() == 0)
				return new ArrayList<MonthlyReportItem>();
			else
				return monthlyReportItems;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return new ArrayList<MonthlyReportItem>();
	}
}
