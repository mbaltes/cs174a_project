package com.dao.domain;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dao.BaseDAO;
import com.domain.Customer;
import com.domain.CustomerStatus;

public class CustomerStatusDAO extends BaseDAO {

	public List<CustomerStatus> findAll() {
		String query = "SELECT * FROM CustomerStatus";

		try {
			List<CustomerStatus> customerStatuses = query(CustomerStatus.class, query, true);
			if (customerStatuses == null)
				return new ArrayList<CustomerStatus>();
			else
				return customerStatuses;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<CustomerStatus>();
	}

	public CustomerStatus findByCustomer(Customer customer) {
		String query = "SELECT cs.* "
				+ "FROM CustomerStatus cs, CustomerStatusMap csm "
				+ "WHERE cs.statusId = csm.statusId AND csm.customerId = '"
				+ customer.getCustomerId() + "'";
		try {
			List<CustomerStatus> customerStatuses = query(CustomerStatus.class, query, true);
			if (customerStatuses != null && customerStatuses.size() > 0)
				return customerStatuses.get(0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public void update(CustomerStatus customerStatus) {
		String query = "UPDATE CustomerStatus SET statusId=" + customerStatus.getStatusId()
				+ ", name='" + customerStatus.getName() + "',discount="
				+ customerStatus.getDiscount() + ",shipRate="
				+ customerStatus.getShipRate() + ",freeShipping="
				+ customerStatus.getFreeShipping() + ",minimum="
				+ customerStatus.getMinimum() + " WHERE statusId="
				+ customerStatus.getStatusId();
		try {
			query(CustomerStatus.class, query, false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
