package com.dao.domain;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dao.BaseDAO;
import com.domain.Customer;
import com.domain.CustomerStatus;
import com.domain.CustomerStatusMap;

public class CustomerStatusMapDAO extends BaseDAO {
	public List<CustomerStatusMap> findAll() {
		String query = "SELECT * FROM CustomerStatusMap";
		
		try {
			List<CustomerStatusMap> csm = query(CustomerStatusMap.class, query, true);
			if (csm == null) 
				return new ArrayList<CustomerStatusMap>();
			else 
				return csm;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<CustomerStatusMap>();
	}
	public void update(CustomerStatusMap customerStatusMap) {
		String query = "UPDATE CustomerStatusMap SET mapId=" + customerStatusMap.getMapId()
				+ ",customerId='" + customerStatusMap.getCustomerId()
				+ "',statusId=" + customerStatusMap.getStatusId()
				+ " WHERE mapId=" + customerStatusMap.getMapId();
		try {
			query(CustomerStatus.class, query, false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public CustomerStatusMap findByCustomer(Customer customer) {
		String query = "SELECT * FROM CustomerStatusMap CSM WHERE CSM.customerId='" + customer.getCustomerId() + "'";

		try {
			List<CustomerStatusMap> csm = query(CustomerStatusMap.class, query,
					true);

			if (csm == null || csm.size() == 0)
				return null;
			else
				return csm.get(0);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
