package com.dao.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;

import com.dao.BaseDAO;
import com.domain.Catalog;
import com.domain.Customer;


public class CatalogDAO extends BaseDAO {

	public void update(Catalog catalog) {
		String query = "UPDATE Catalog SET stockNumber='"
				+ catalog.getStockNumber() + "',category='"
				+ catalog.getCategory() + "',manufacturer='"
				+ catalog.getManufacturer() + "',model='" + catalog.getModel()
				+ "',warranty=" + catalog.getWarranty() + ",price="
				+ catalog.getPrice() + " WHERE stockNumber='"
				+ catalog.getStockNumber() + "'";
		try {
			query(Catalog.class, query, false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<String> findAllCategories() {
		String query = "SELECT DISTINCT C.category FROM Catalog C";
		
		try {
			List<String> categories = new ArrayList<String>();
			
			ResultSet resultSet = queryWithoutMap(query);
			while (resultSet.next())
			{
				categories.add(resultSet.getString(1));
			}
			resultSet.close();
			
			if (categories != null && categories.size() > 0)
				return categories;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return new ArrayList<String>();
	}
	
	public List<Catalog> findAll() {
		String query = "SELECT * FROM Catalog";
		List<Catalog> returnValue = null;
		try {
			returnValue = query(Catalog.class,query, true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnValue = new LinkedList<Catalog>();
		}
		return returnValue;
	}
	
	///search for catalog entries by parameters and by description
	public List<Catalog> findWhereDescriptions(String where) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT c.* FROM Catalog c, Descriptions d WHERE ");
		sb.append(where);
		String q = sb.toString();
		System.out.println(q);
		try 
		{
			List<Catalog> returnValue = query(Catalog.class,q, true);
			if (returnValue == null)
				return new ArrayList<Catalog>();
			else
				return returnValue;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new LinkedList<Catalog>();
	}
	
	///search for catalog entreis only by parameters
	public List<Catalog> findWhere(String where) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT c.* FROM Catalog c WHERE ");
		sb.append(where);
		try 
		{
			List<Catalog> returnValue = query(Catalog.class,sb.toString(), true);
			if (returnValue == null)
				return new ArrayList<Catalog>();
			else
				return returnValue;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new LinkedList<Catalog>();
	}
	
	public List<Catalog> findAccessoriesOf(Catalog c) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM Catalog c, Accessories a ");
		sb.append("WHERE c.stockNumber = a.child AND ");
		sb.append("a.parent = '");
		sb.append(c.getStockNumber());
		sb.append("' ORDER BY c.stockNumber");
		try 
		{
			List<Catalog> returnValue = query(Catalog.class,sb.toString(), true);
			if (returnValue == null)
				return new ArrayList<Catalog>();
			else
				return returnValue;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new LinkedList<Catalog>();
	}
	
	public Catalog findByStockNumber(String stockNumber)
	{
		String query = "SELECT * FROM Catalog WHERE stockNumber = '" + stockNumber + "'";
	
		try {
			List<Catalog> catalogItems = query(Catalog.class, query, true);
			if (catalogItems == null || catalogItems.size() == 0)
			{
				return null;
			}
			else
			{
				return catalogItems.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}