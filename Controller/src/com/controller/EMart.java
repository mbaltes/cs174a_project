package com.controller;

import java.io.IOException;
import java.util.Scanner;

import com.controller.emart.CustomerInterface;
import com.controller.emart.ManagerInterface;
import com.controller.emart.UserLoginInterface;
import com.domain.Customer;
import com.service.CartService;
import com.service.CatalogService;
import com.service.CustomerService;
import com.service.CustomerStatusMapService;
import com.service.CustomerStatusService;
import com.service.OrderItemService;
import com.service.OrderService;

public class EMart {
	public static CustomerService customerService;
	public static CustomerStatusService customerStatusService;
	public static CustomerStatusMapService customerStatusMapService;
	public static CatalogService catalogService;
	public static OrderService orderService;
	public static OrderItemService orderItemService;
	public static CartService cartService;
	public static Customer customer;
	public static Scanner scanner;

	public EMart() {
		customerService = new CustomerService();
		catalogService = new CatalogService();
		orderService = new OrderService();
		orderItemService = new OrderItemService();
		cartService = new CartService();
		customerStatusService = new CustomerStatusService();
		customerStatusMapService = new CustomerStatusMapService();
		scanner = new Scanner(System.in);
	}

	public void chooseInterface() throws IOException {
		String params = "";

		System.out.println("|---------------------------------|");
		System.out.println("|         WELCOME TO EMART        |");
		System.out.println("|---------------------------------|");
		System.out.println("  c - Customer Login");
		System.out.println("  m - Manager");
		while (!params.equals("e")) {
			System.out.print("\nChoice: ");
			params = scanner.nextLine();

			switch (params) {
			case "c":
				UserLoginInterface.printLoginScreen();
				if (customer != null)
					CustomerInterface.customerInterface();
				break;
			case "m":
				UserLoginInterface.printLoginScreen();
				if (customer != null)
				{
					if (customer.getManager() > 0)
						ManagerInterface.managerInterface();
					else
						System.out.println("\nYou are not a manager.\n");
				}
				break;
			default:
				System.out.println("Not a valid choice.");
			}
		}
	}
}