package com.controller.emart;

import java.util.List;

import com.controller.EMart;
import com.domain.Cart;
import com.domain.Catalog;

public class ShoppingCartInterface extends EMart {
	public static void addItemToCart() {
		String stockNumber;
		int quantity;
		System.out.println("To add an item to the cart, enter the following information.");
		System.out.print("Stock number: ");
		stockNumber = scanner.nextLine();
		
		//Check if the item exists
		Catalog catalog = catalogService.findByStockNumber(stockNumber);
		if (catalog == null) 
			System.out.println("\nNo item was found with that stock number.\n");
		else
		{
			System.out.print("Quantity: ");
			quantity = scanner.nextInt();
			
			//Add to cart
			Cart cart = new Cart();
			cart.setCart(0, customer.getCustomerId(),stockNumber, quantity);
			cartService.insert(cart);
			
			System.out.println();
		}
	}
	public static void deleteItemFromCart() {
		String stockNumber;
		
		System.out.println("To delete an item from the cart, enter the following information.");
		System.out.print("Stock number: ");
		stockNumber = scanner.nextLine();
		
		//Check if the item is in the cart
		if (cartService.findByStockNumber(stockNumber) == null)
			System.out.println("\nNo item was found with that stock number.\n");
		else
		{
			cartService.delete(customer.getCustomerId(), stockNumber);
			System.out.println("\nItem was removed from cart.\n");
		}
		//Cart cart = cartService.delete(customer.getCustomerId(), stockNumber);
	}
	public static void viewAllItemsInCart() {
		System.out.println("|---------------------------------|");
		System.out.println("|           SHOPPING CART         |");
		System.out.println("|---------------------------------|");
		
		List<Cart> cartItems = cartService.findAll(customer);
		if (cartItems.size() > 0)
		{
			Cart.printHeader();
			for (Cart cart : cartItems)
			{
				cart.printRowDisplay();
				System.out.println();
			}
		}
		else
			System.out.println("No items in shopping cart.\n");
	}

	public static void purchaseCart() {
		List<Cart> cartItems = cartService.findAll(customer);
		
		if (cartItems.size() == 0) {
			System.out.println("\nNo items in shopping cart, therefore we cannot checkout your cart.\n");
		} else {
			cartService.checkout(customer);
		}
	}
}
