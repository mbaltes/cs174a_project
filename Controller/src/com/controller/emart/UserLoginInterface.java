package com.controller.emart;

import java.util.Scanner;

import com.controller.EMart;
import com.domain.Customer;

public class UserLoginInterface extends EMart {
	public static void printLoginScreen() {
		String[] credentials = new String[2];
		
		boolean isValid = false;

		System.out.println("|---------------------------------|");
		System.out.println("|               LOGIN             |");
		System.out.println("|---------------------------------|");
		System.out.println("  Please follow the prompts to login into your account.");
		System.out.println();
		
		while (!isValid) {
			System.out.print("  Customer ID: ");
			credentials[0] = scanner.nextLine();
			System.out.print("  Password: ");
			credentials[1] = scanner.nextLine();
			
			Customer customerLoggedIn = customerService.findByCustomerId(credentials[0]);
			if (customerLoggedIn == null)
			{
				System.out.println("  ERR: Password was invalid. Try again.");
				System.out.println();
			}
			else
			{
				//We know the user exists, lets see if their passwords match
				if (customerLoggedIn.getPassword().equals(credentials[1]))
				{
					//Set global customer
					customer = customerLoggedIn;
					System.out.println("  Login Successful!");
					System.out.println();
					isValid = Boolean.TRUE;
					break;
				}
				else
				{
					System.out.println("  ERR: Password was invalid. Try again.");
					System.out.println();
				}
			}
		}
	}
}
