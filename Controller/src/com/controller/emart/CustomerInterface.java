package com.controller.emart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.controller.EMart;
import com.domain.Cart;
import com.domain.Catalog;
import com.domain.Customer;
import com.domain.Description;
import com.domain.Order;
import com.domain.OrderItem;

public class CustomerInterface extends EMart {
	public static void customerInterface() throws IOException {
		System.out.println("|---------------------------------|");
		System.out.println("|       CUSTOMER INTERFACE        |");
		System.out.println("|---------------------------------|");
		System.out.println("  o - All previous orders");
		System.out.println("  f - Find previous order by order id");
		System.out.println("  r - Rerun previous order");
		System.out.println("  s - Search for items");
		System.out.println("|---------------------------------|");
		System.out.println("  a - Add item to cart");
		System.out.println("  d - Delete item from cart");
		System.out.println("  v - View all items in cart");
		System.out.println("  p - Purchase items");
		System.out.println("  e - Exit\n");
		String params = "";
		while (!params.equals("e")) {
			System.out.print("\nChoice: ");
			
			params = scanner.nextLine();
			switch (params) {
			case "o":
				printAllPreviousOrders(customer);
				break;
			case "f":
				findPreviousOrderByOrderId();
				break;
			case "r":
				rerunPreviousOrder();
				break;
			case "s":
				searchInterface();
				break;
			case "a":
				ShoppingCartInterface.addItemToCart();
				break;
			case "d":
				ShoppingCartInterface.deleteItemFromCart();
				break;
			case "v":
				ShoppingCartInterface.viewAllItemsInCart();
				break;
			case "p":
				ShoppingCartInterface.purchaseCart();
				break;
			case "e":
				System.exit(0);
				break;
			case "":
				break;
			default:
				System.out.println("Not a valid choice..");
				break;
			}
		}
	}

	public static void searchInterface() throws IOException {
		System.out.println("|---------------------------------|");
		System.out.println("|            SEARCH               |");
		System.out.println("|---------------------------------|");
		System.out.println("How to search: enter in a combination of categories below");
		System.out.println("Example input: mca    \n(this means search by manufacturer, category, and show accessories of all search results.)");
		System.out.println("  n - Stock Number");
		System.out.println("  c - Category");
		System.out.println("  m - Manufacturer");
		System.out.println("  o - Model");
		System.out.println("  d - Description attribute&value");
		System.out.println("  a - Show accessories of all items");
		System.out.print("\nSearch by: ");
		
		InputStreamReader sr =new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(sr);
		
		String params = br.readLine();
		String buffer;
		String attribute = null;
		String value = null;
		
		LinkedList<String> fields = new LinkedList<String>();
		LinkedList<String> values = new LinkedList<String>();
		LinkedList<String> attributes = new LinkedList<String>();
		LinkedList<String> attributeValues = new LinkedList<String>();
		Map<String, List<Catalog> > accessoryMap = null;
		
		//TODO: check for multiple instances.
		
		for (char c : params.toCharArray()) {
			if( c == 'n' ) {
				System.out.print("Stock Number: ");
				buffer = br.readLine();
				fields.add("c.stockNumber");
				values.add("'" + buffer + "'");
			}
			if( c == 'c') {
				System.out.print("Category: ");
				buffer = br.readLine();
				fields.add("c.category");
				values.add("'" + buffer + "'");
			}
			if( c == 'm') {
				System.out.print("manufacturer: ");
				buffer = br.readLine();
				fields.add("c.manufacturer");
				values.add("'" + buffer + "'");
			}
			if( c == 'o') {
				System.out.print("model: ");
				buffer = br.readLine();
				fields.add("c.model");
				values.add("'" + buffer + "'");
			}
			if( c == 'a' ) {
				accessoryMap = new HashMap<String, List<Catalog> >();
			}
			if( c == 'd' ) {
				System.out.print("Description attribute: ");
				attribute = br.readLine();
				System.out.print("            value: ");
				value = br.readLine();
				attributes.add(attribute);
				attributeValues.add(value);
			}
		}
		
		List<Catalog> results = catalogService.search(fields, values, attributes, attributeValues, accessoryMap);
		
		//now display results
		System.out.println();
		Catalog.printHeader();
		for(Catalog c : results) {
			c.printRowDisplay();
			if(c.descriptions != null) {
				Description.printHeader();
				for(Description d : (c.descriptions)) {
					d.printRowDisplay();
				}
			}
			if(accessoryMap != null) {
				List<Catalog> accList = accessoryMap.get(c.getStockNumber());
				if(accList != null && accList.size() > 0) {
					System.out.printf("\n%-15s %-15s\n", "", "Accessories");
					System.out.printf("%-15s %-15s %-15s %-15s\n", "", "Stock Number", "Manufacturer", "Model");
					System.out.printf("%-15s %-15s\n", "", "--------------------------------");
					for(Catalog acc : accList) {
						System.out.printf("%-15s %-15s %-15s %-15s\n", "", acc.getStockNumber(), acc.getManufacturer(), acc.getModel());
					}
				}
			}
			System.out.println("\n");
		}
		
	}

	public static void printAllPreviousOrders(Customer customer) {
		List<Order> orders = orderService.findByCustomer(customer);

		System.out.println("|---------------------------------|");
		System.out.println("|         PREVIOUS ORDERS         |");
		System.out.println("|---------------------------------|");

		if (orders.size() > 0) {
			Order.printHeader();
			for (Order order : orders) {
				order.printRowDisplay();
			}
		} else {
			System.out.println("No orders have been made.");
		}
		System.out.println();
	}

	public static void rerunPreviousOrder() {
		System.out.print("\nPrevious order id: ");
		String params = scanner.nextLine();

		Order order = orderService.findByOrderId(Integer.parseInt(params));
		if (order == null)
			System.out.println("There is no order with that id.\n");
		else {
			// Get all the items in the order and process a new checkout
			List<OrderItem> orderItems = orderItemService.findAllByOrder(order);

			// Create a new checkout

			for (OrderItem orderItem : orderItems) {
				Cart cart = new Cart();
				cart.setCart(0, customer.getCustomerId(), orderItem.getStockNumber(), orderItem.getQuantity());
				cartService.insert(cart);
			}
			// Now checkout
			cartService.checkout(customer);
		}
	}

	public static void findPreviousOrderByOrderId() {
		System.out.print("\nOrder id: ");
		String params = scanner.nextLine();

		Order order = orderService.findByOrderId(Integer.parseInt(params));

		if (order == null)
			System.out.println("There is no order with that id.\n");
		else {
			System.out.println("Found order with the following information:");
			Order.printHeader();
			order.printRowDisplay();
			System.out.println();
		}
	}
}
