package com.controller.emart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import com.controller.EMart;
import com.domain.Catalog;
import com.domain.Customer;
import com.domain.CustomerStatus;
import com.domain.CustomerStatusMap;
import com.domain.ReplenishmentItem;
import com.domain.ReplenishmentOrder;
import com.domain.ShippingItem;
import com.domain.ShippingNotice;
import com.domain.report.MonthlyReport;
import com.service.InventoryService;

public class ManagerInterface extends EMart {
	/*Print monthly summary of sales: the amount (quantity, price) of sale per product, per category, and the
	customer who did the most purchase.*/
	
	public static void managerInterface() throws IOException {
		System.out.println("|---------------------------------|");
		System.out.println("|        MANAGER INTERFACE        |");
		System.out.println("|---------------------------------|");
		System.out.println("  p - Print Monthly Summary Sales");
		System.out.println("  s - Customer Status Adjustment");
		System.out.println("  o - Send order to manufacturer");
		System.out.println("  c - Change item price");
		System.out.println("  d - Delete sales transactions");
		System.out.println("  e - Exit\n");
		String params = "";
		while (!params.equals("e")) {
			System.out.print("\nChoice: ");
			params = scanner.nextLine();
			switch (params) {
			case "p":
				printMonthlySummarySales();
				break;
			case "s":
				printCustomerStatusAdjustment();
				break;
			case "o":
				printSendOrderToManufacturer();
				break;
			case "c": 
				printChangePrice();
				break;
			case "d":
				printDeleteSalesTransactions();
				break;
			case "e":
				System.exit(0);
				break;
			case "":
				break;
			default:
				System.out.println("Not a valid choice..");
				break;
			}
		}
	}
	
	public static void printMonthlySummarySales() {
		String sDate;
		String eDate;
		
		System.out.println("|---------------------------------|");
		System.out.println("|       MONTHY SALES SUMMARY      |");
		System.out.println("|---------------------------------|");
		System.out.print("Please enter start date (Year/Month/Day)): ");
		sDate = scanner.nextLine();
		System.out.print("Please enter end date (Year/Month/Day): ");
		eDate = scanner.nextLine();
		System.out.println();
		
		MonthlyReport monthlyReport = orderService.generateMonthlyReport(sDate, eDate);
		monthlyReport.printRowDisplay();
	}
	
	//*Customer status adjustment according to the sales in the month.
	public static void printCustomerStatusAdjustment() {
		String params;
		System.out.println("|---------------------------------|");
		System.out.println("|    CUSTOMER STATUS ADJUSTMENT   |");
		System.out.println("|---------------------------------|");
		System.out.println("  a - Update all Customer Statuses automatically");
		System.out.print("  s - Update a specific customers status");
		System.out.println();
		System.out.print("\nChoice: ");
		params = scanner.nextLine();
		
		switch (params) {
		case "a":
			customerStatusService.updateAllCustomerStatusDiscountStatus();
			break;
		case "s":
			updateACustomerStatus();
			break;
		default:
			System.out.print("\nNo such choice. \nReturning to main menu..\n");
			break;
		}
	}
	
	public static void updateACustomerStatus() {
		String customerId;
		int newStatusId;
		int oldStatusId;
		
		System.out.println("To update a customer's status, please enter the following.");
		System.out.print("CustomerID: ");
		customerId = scanner.nextLine();
		
		Customer customerToUpdate = customerService.findByCustomerId(customerId);
		
		if (customerToUpdate != null)
		{
			CustomerStatusMap customerStatusMap = customerStatusMapService.findByCustomer(customerToUpdate);
			CustomerStatus currentCustomerStatus = customerStatusService.findByCustomer(customerToUpdate);
			List<CustomerStatus> customerStatuses = customerStatusService.findAll();
			
			System.out.println("The customers current status....");
			CustomerStatus.printHeader();
			currentCustomerStatus.printRowDisplay();
			
			System.out.println("Available statuses....");
			CustomerStatus.printHeader();
			for (CustomerStatus customerStatus : customerStatuses) {
				customerStatus.printRowDisplay();
			}
			System.out.println();
			System.out.println("To change the customers current status, enter the new status id you want the customer to have.");
			System.out.print("New status id: ");
			newStatusId = scanner.nextInt();
			
			//Set the new customer status id
			oldStatusId = customerStatusMap.getStatusId();
			customerStatusMap.setStatusId(newStatusId);
			customerStatusMapService.update(customerStatusMap);
			System.out.println("Customer status id has been updated from " + oldStatusId + "->" + newStatusId);
		} else {
			System.out.print("\nDid not find a customer with that id.\n Returning to main menu.\n");
		}
	}
	
	//*Send an order to a manufacturer (shipment will go to eDEPOT directly).
	public static void printSendOrderToManufacturer() throws IOException {
		InventoryService inventoryService = new InventoryService();
		
		InputStreamReader sr =new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(sr);
		
		ReplenishmentOrder order = new ReplenishmentOrder();
		ShippingNotice notice = new ShippingNotice();
		
		List<ReplenishmentItem> rItems = new LinkedList<ReplenishmentItem>();
		List<ShippingItem> sItems = new LinkedList<ShippingItem>();
		
		order.setOrderId(inventoryService.generateReplenishmentOrderId());
		notice.setNoticeId(inventoryService.generateShippingNoticeId());
		notice.setShippingCompany("FedEx");
		
		String input;
		Integer n;
		
		System.out.println("*** Prepare replenishment order ***\n");
		System.out.print("Date stamp yyyy/mm/dd: ");
		order.setTime(br.readLine());
		notice.setTime(order.getTime());
		System.out.print("Manufacturer: ");
		order.setManufacturer(br.readLine());
		System.out.print("How many different items?: ");
		n = Integer.parseInt(br.readLine());
		
		int rid = inventoryService.generateReplenishmentItemId();
		int sid = inventoryService.generateShippingItemId();
		
		for(int i = 0; i < n; i++) {
			 ReplenishmentItem ritem = new ReplenishmentItem();
			 ShippingItem sitem = new ShippingItem();
			 
			 ritem.setItemId(rid++);
			 ritem.setOrderId(order.getOrderId());
			 
			 sitem.setItemId(sid++);
			 sitem.setNoticeId(notice.getNoticeId());
			 
			 ritem.setManufacturer(order.getManufacturer());
			 sitem.setManufacturer(order.getManufacturer());
			 
			 System.out.print("Model: ");
			 ritem.setModel(br.readLine());
			 sitem.setModel(ritem.getModel());
			 
			 System.out.print("Quantity: ");
			 ritem.setQuantity(Integer.parseInt(br.readLine()));
			 sitem.setQuantity(ritem.getQuantity());
			 
			 rItems.add(ritem);
			 sItems.add(sitem);
		}
		
		System.out.print("\nGenerate shipping notice? y/n: ");
		input = br.readLine();
		
		inventoryService.persistReplenishmentOrder(order, rItems);
		if(input.equals("y")) {
			inventoryService.persistShippingNotice(notice, sItems);
		}
	}
	
	//*Change the price of an item.
	public static void printChangePrice() {
		String stockNumber;
		Float newPrice;
		Catalog catalogItem;
		
		System.out.println("To change the price of an item, please enter the following.");
		System.out.print("Stock Number: ");
		stockNumber = scanner.nextLine();
		
		//See if the item exists
		catalogItem = catalogService.findByStockNumber(stockNumber);
		
		if (catalogItem == null) {
			System.out.println("\nNo item was found with that stock number.\n");
		}
		else
		{
			System.out.print("Price (Current: " + catalogItem.getPrice() + "): ");
			newPrice = scanner.nextFloat();
			if (newPrice >= 0)
			{
				catalogItem.setPrice(newPrice);
				catalogService.update(catalogItem);
			}
			else
				System.out.print("\nInvalid price. Not saving changes.\n");
		}
	}
	/*Delete all sales transactions that are no longer needed in computing customer status. All orders need to
	be stored unless they are explicitly deleted.*/
	public static void printDeleteSalesTransactions() {
		orderService.deleteUnnecessarySalesTransactions();
	}
}
