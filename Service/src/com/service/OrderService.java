package com.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.dao.domain.CatalogDAO;
import com.dao.domain.MonthlyReportDAO;
import com.dao.domain.OrderDAO;
import com.domain.Customer;
import com.domain.Order;
import com.domain.report.MonthlyReport;

public class OrderService {
	private OrderDAO orderDAO;
	private CatalogDAO catalogDAO;
	private MonthlyReportDAO monthlyReportDAO;

	public OrderService() {
		orderDAO = new OrderDAO();
		catalogDAO = new CatalogDAO();
		monthlyReportDAO = new MonthlyReportDAO();
	}

	public List<Order> findByCustomer(Customer customer) {
		return orderDAO.findByCustomer(customer);
	}
	
	public Order findByOrderId(int orderId) {
		return orderDAO.findByOrderId(orderId);
	}
	
	public int findMaxOrderId() {
		return orderDAO.findMaxOrderId().getOrderId();
	}
	
	public List<Order> findAllUnprocessed() {
		return orderDAO.findAllUnprocessed();
	}
	
	public MonthlyReport generateMonthlyReport(String startDate, String endDate) {
		MonthlyReport monthlyReport = new MonthlyReport();
		
		List<String> categories = catalogDAO.findAllCategories();
		
		monthlyReport.setHighestPayingCustomer(orderDAO.findHighestSpendingCustomer(startDate, endDate));
		//For each category, find the values.
		for (String category : categories) {
			monthlyReport.getCategoryToReportMap().put(category, monthlyReportDAO.findMonthlyReportItemsByCategory(category, startDate, endDate));
		}
		return monthlyReport;
	}
	
	public void markOrderProcessed(Order order) {
		orderDAO.markOrderProcessed(order);
	}
	
	public void deleteUnnecessarySalesTransactions() {
		orderDAO.deleteUnnecessarySalesTransactions();
	}
}
