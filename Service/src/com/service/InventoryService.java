package com.service;

import java.util.Date;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.dao.domain.InventoryDAO;
import com.domain.Inventory;
import com.domain.Order;
import com.domain.OrderItem;
import com.domain.ReplenishmentItem;
import com.domain.ReplenishmentOrder;
import com.domain.ShippingItem;
import com.domain.ShippingNotice;

///Inventory Service manages all eDepot stuff.
public class InventoryService {
	InventoryDAO inventoryDAO;
	OrderItemService orderItemService;
	OrderService orderService;
	public InventoryService() {
		inventoryDAO = new InventoryDAO();
		orderItemService = new OrderItemService();
		orderService = new OrderService();
	}
	
	public Inventory findByStockNumber(String stockNumber) {
		return inventoryDAO.findByStockNumber(stockNumber);
	}
	
	public Inventory findByLocation(String location) {
		return inventoryDAO.findByStockNumber(location);
	}
	
	public Inventory findByManufacturerModel(String manufacturer, String model) {
		return inventoryDAO.findByManufacturerModel(manufacturer, model);
	}
	
	public List<Inventory> findDepletedByManufacturer(String manufacturer) {
		return inventoryDAO.findDepletedByManufacturer(manufacturer);
	}
	
	///Shipping notice section.
	
	public int generateShippingNoticeId() {
		ShippingNotice max = inventoryDAO.getShippingNoticeHighestPK();
		if(max != null) {
			return max.getNoticeId() + 1;
		}
		return 1;
	}
	
	public int generateShippingItemId() {
		ShippingItem max = inventoryDAO.getShippingItemHighestPK();
		if(max != null) {
			return max.getItemId() + 1;
		}
		return 1;
	}
	
	public ShippingNotice findByNoticeId(int noticeId) {
		return inventoryDAO.findShippingNoticeById(noticeId);
	}
	
	public String generateNewStockNumber() {
		String result;
		do {
			result = "SN";
			int min, max;
			min = 1;
			max = 99999;
			Integer num = max + (int)(Math.random() * ((max - min) + 1));
			String numStr = num.toString();
			result = result + ("00000" + numStr).substring(numStr.length());
		} while(findByStockNumber(result) != null); //keep checking until new stock number
		return result;
	}
	
	public String generateNewLocation() {
		String result;
		do {
			Random r = new Random();
			Character c = (char) (r.nextInt(26) + 'A');
			result = c.toString();
			int min, max;
			min = 1;
			max = 9;
			Integer num = max + (int)(Math.random() * ((max - min) + 1));
			result = result + num.toString();
		} while(findByLocation(result) != null); //keep checking until new stock number
		return result;
	}
	
	public List<ShippingNotice> findUnreceivedShippingNotices() {
		return inventoryDAO.findUnreceivedShippingNotices();
	}
	
	public List<ShippingItem> findItemsByNoticeId(int noticeId) {
		return inventoryDAO.findItemsByNoticeId(noticeId);
	}
	
	public List<Inventory> receiveShipment(ShippingNotice notice) throws NumberFormatException, IOException {
		//to do this, we first must get the list of all Shipping Items for this order.
		
		List<ShippingItem> items = findItemsByNoticeId(notice.getNoticeId());
		List<Inventory> results = null;
		
		if(items != null) {
			results = new LinkedList<Inventory>();
			//ok, so for each of these items we must get the inventory item with the same thing.
			for(ShippingItem item : items) {
				Inventory inv = findByManufacturerModel(item.getManufacturer(), item.getModel());
				
				//check if this is a new item
				
				if(inv != null) { //this is not a new item
					//this is an existing item. Let's adjust the stuff.
					inv.setQuantity(inv.getQuantity() + item.getQuantity());
					
					//adjust replenishment information
					int newReplen = inv.getReplenishment() - item.getQuantity();
					if(newReplen < 0) newReplen = 0;
					inv.setReplenishment(newReplen);
					
					//persist this object.
					inventoryDAO.updateInventoryQuantityReplenishments(inv);
					results.add(inv);
				}
			}
		}
		
		//now that the inventory is updated, we can mark this as received.
		inventoryDAO.markShippingNoticeReceived(notice);
		
		return results;
	}
	
	//MODIFICATION ACTIONS
	//----------------------------------------------------------
	public void persistInventory(Inventory inv) {
		inventoryDAO.persistInventory(inv);
	}
	
	public void persistShippingNotice(ShippingNotice notice, List<ShippingItem> items) {
		inventoryDAO.persistShippingNotice(notice);
		for(ShippingItem item : items) {
			inventoryDAO.persistShippingItem(item);
		}
	}

	public void persistShippingNoticeAndUpdateInventory(ShippingNotice notice, List<ShippingItem> items) {
		inventoryDAO.persistShippingNotice(notice);
		for(ShippingItem item : items) {
			if(notice.getReceived() == 0) {
					//udpate replenishment information.
					Inventory inv = findByManufacturerModel(item.getManufacturer(), item.getModel());
					if(inv != null) {
						inv.setReplenishment(inv.getReplenishment() + item.getQuantity());
						updateInventoryQuantityReplenishments(inv);
					}
				}
			inventoryDAO.persistShippingItem(item);
		}
	}
	
	public void persistReplenishmentOrder(ReplenishmentOrder order, List<ReplenishmentItem> items) {
		inventoryDAO.persistReplenishmentOrder(order);
		for(ReplenishmentItem item : items) {
			inventoryDAO.persistReplenishmentItem(item);
		}
	}
	
	public void markShippingNoticeReceived(ShippingNotice notice) {
		inventoryDAO.markShippingNoticeReceived(notice);
	}
	
	public void updateInventoryQuantityReplenishments(Inventory inv) {
		inventoryDAO.updateInventoryQuantityReplenishments(inv);
	}
	
	//Order filling
	//----------------------------------------------------------
	public void fillOrders(List<Order> orders) {
		for(Order order : orders) {
			if(!fillOrder(order)) {
				System.out.println("Order number " + order.getOrderId() + " could not be filled due to insufficient quantity in warehouse.");
				System.out.println("However, automatic replenishments may have been submitted");
				System.out.println("Order more items and/or receive a shipment to fill this order.");
			}
		}
	}
	
	public Boolean fillOrder(Order order) {
		//to fill an order, we must do the following
		//1) get all OrderItem objects
		//2) for each Order Item
		//
		//		- Let X(M) -> N be a map from manufacturers to number of items from that manufacturer which need replenishment.
		//		- new inventory.quantity = max(0, inventory.quantity - orderItem.quantity)
		//		- if new inventory.quantity + inventory.replenishment < inventory.minimum
		//			- then X(inventory.manufacturer)++
		//		- for each pair (M,N) where N >= 3
		//			- for each item by manufacturer M where quantity + replenishment < max
		//				- add to replenishment order for the amount max - (quantity + replenishment)
		
		//assume order can be filled until proven otherwise by an item having a quantity that is too low to fill the order.
		Boolean orderCanBeFilled = true;
		
		//get OrderItem objects
		List<OrderItem> orderItems = orderItemService.findAllByOrder(order);
		
		//lowMap is a map from manufacturers to list of items needing to be replenished that have that manufacturer.
		HashMap<String, List<Inventory> > lowMap = new HashMap<String, List<Inventory> >();
		
		List<Inventory> orderInventory = new LinkedList<Inventory>();
		
		//handle each item.
		for(OrderItem item : orderItems) {
			Inventory inv = findByStockNumber(item.getStockNumber());
			
			
			//IMPORTANT PIECE OF BUSINESS LOGIC!!!
			//reject if there are not enough items in the warehouse to fill it.
			//BUT, STILL try to replenish the stocks to maximum to be able to fill this order.
			
			int newQuantity = inv.getQuantity() - item.getQuantity();
			if(inv.getQuantity() < item.getQuantity()) {
				orderCanBeFilled = false;
			} else {
				if(newQuantity < 0) newQuantity = 0;
				inv.setQuantity( newQuantity );
			}
			
			//check if it goes below min
			if(newQuantity + inv.getReplenishment() < inv.getMin())
			{
				List<Inventory> depleted = null;
				//item below minimum quantity, see if this manufacturer needs to be replenished.
				if(lowMap.containsKey(inv.getManufacturer())) {
					depleted = lowMap.get(inv.getManufacturer());
					depleted.add(inv);
				} else {
					depleted = findDepletedByManufacturer(inv.getManufacturer());
					if(depleted == null) {
						depleted = new LinkedList<Inventory>();
					}
					depleted.add(inv);
					lowMap.put(inv.getManufacturer(), depleted);
				}
			}
			
			//add this inventory item to list of items to be updated.
			orderInventory.add(inv);
		}
		
		//mark order as filled
		if(orderCanBeFilled) {
			for(Inventory inv : orderInventory) {
				updateInventoryQuantityReplenishments(inv);
			}
			orderService.markOrderProcessed(order);
		}
		//replenish depleted items.
		replenishItems(lowMap);
		
		return orderCanBeFilled;
	}
	
	public void replenishItems(Map<String, List<Inventory> > itemMap) {
		//to make a replenishment order for a set of items, do the following.
		//- Create a valid ReplenishmentOrder object
		//- Create a ShippingNotice which corresponds to the order with UPS as the shipper.
		//- Create a ReplenishmentItem for each item and attach it to the order
		//- Create a ShippingItem for each item and attach it to the shipping notice.
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		
		Iterator it = itemMap.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			List<Inventory> items = (List<Inventory>)pair.getValue();
			
			//In the specification, a replenishment order is only issued when 3 items from a manufacturer are depleted.
			if(items.size() >= 3) {
				//create a valid replenishment order
				ReplenishmentOrder order = new ReplenishmentOrder();
				order.setOrderId(generateReplenishmentOrderId());
				order.setManufacturer((String)pair.getKey());
				order.setTime(dateFormat.format(date));
				order.setReceived(1);
				
				//create a valid shipping notice that meets this replenishment order.
				//default to UPS as shipper
				ShippingNotice notice = new ShippingNotice();
				notice.setNoticeId(generateShippingNoticeId());
				notice.setShippingCompany("UPS");
				notice.setTime(dateFormat.format(date));
				
				List<ShippingItem> shippingItems = new LinkedList<ShippingItem>();
				List<ReplenishmentItem> replenishmentItems = new LinkedList<ReplenishmentItem>();
				
				int rPK = generateReplenishmentItemId();
				int sPK = generateShippingItemId();
				
				//now populate these.
				for(Inventory inv : items) {
					//only replenish item if it's below max.
					if(inv.getQuantity() + inv.getReplenishment() < inv.getMax()) {
						//calculate quantity of replenishment order for this item.
						int quantity = inv.getMax() - (inv.getQuantity() + inv.getReplenishment());
						
						//create replenishment item and attach to the replenishment order
						ReplenishmentItem rItem = new ReplenishmentItem();
						rItem.setItemId(rPK++);
						rItem.setOrderId(order.getOrderId());
						rItem.setManufacturer(inv.getManufacturer());
						rItem.setModel(inv.getModel());
						rItem.setQuantity(quantity);
						
						//create shipping notice item and attach to the shipping notice
						ShippingItem sItem = new ShippingItem();
						sItem.setItemId(sPK++);
						sItem.setNoticeId(notice.getNoticeId());
						sItem.setManufacturer(inv.getManufacturer());
						sItem.setModel(inv.getModel());
						sItem.setQuantity(quantity);
						
						//add these both to their respective lists
						shippingItems.add(sItem);
						replenishmentItems.add(rItem);
					}
				}
				
				//persist the order and notice
				persistReplenishmentOrder(order, replenishmentItems);
				persistShippingNoticeAndUpdateInventory(notice, shippingItems);
			}
		}
	}
	
	public int generateReplenishmentOrderId() {
		ReplenishmentOrder max = inventoryDAO.getReplenishmentOrderHighestPK();
		if(max != null) {
			return max.getOrderId() + 1;
		}
		return 1;
	}
	
	public int generateReplenishmentItemId() {
		ReplenishmentItem max = inventoryDAO.getReplenishmentItemHighestPK();
		if(max != null) {
			return max.getItemId() + 1;
		}
		return 1;
	}
}
