package com.service;

import java.util.List;
import java.util.Map;

import com.dao.domain.CatalogDAO;
import com.dao.domain.DescriptionsDAO;
import com.domain.Catalog;

public class CatalogService {
	private CatalogDAO catalogDAO;
	private DescriptionsDAO descriptionsDAO;

	public CatalogService() {
		catalogDAO = new CatalogDAO();
		descriptionsDAO = new DescriptionsDAO();
	}

	public List<Catalog> findAll() {
		return catalogDAO.findAll();
	}
	
	public void update(Catalog catalog) {
		catalogDAO.update(catalog);
	}

	public Catalog findByStockNumber(String stockNumber) {
		return catalogDAO.findByStockNumber(stockNumber);
	}

	public List<Catalog> search(List<String> fields, List<String> values, 
							    List<String> attributes, List<String> attributeValues, 
							    Map<String, List<Catalog> > accessoryMap) {
		StringBuilder sb = new StringBuilder();
		int iterationBound = Math.min(fields.size(), values.size());
		for (int x = 0; x < iterationBound; x++) {
			sb.append(fields.get(x));
			sb.append(" = ");
			sb.append(values.get(x));
			if (x != iterationBound - 1)
				sb.append(" AND ");
		}
		
		List<Catalog> items;
		//if attributes isn't empty then we are also searching by descriptions
		if(attributes.size() > 0 ) {
			//if fields.size == 0 then we are only searching by descriptions and don't need this and.
			if(fields.size() > 0) {
				sb.append(" AND ");
			}
			sb.append("c.stockNumber = d.stockNumber AND (");
			for(int x = 0; x < attributes.size(); x++) {
				sb.append("d.attribute = '");
				sb.append(attributes.get(x));
				sb.append("' AND ");
				sb.append("d.value = '");
				sb.append(attributeValues.get(x));
				sb.append("'");
				if(x != attributes.size()-1) {
					sb.append(" OR ");
				}
			}
			sb.append(")");
			items = catalogDAO.findWhereDescriptions(sb.toString());
		} else {
			items = catalogDAO.findWhere(sb.toString());
		}
		//load descriptions into the objects.
		for(Catalog c : items) {
			c.descriptions = descriptionsDAO.findByStockNumber(c.getStockNumber());
		}
		
		//get accessories
		if(accessoryMap != null) {
			for(Catalog c : items)
			{
				List<Catalog> accessories = catalogDAO.findAccessoriesOf(c);
				accessoryMap.put(c.getStockNumber(), accessories);
			}
		}
		
		return items;
	}
}
