package com.service;

import java.util.List;

import com.dao.domain.CustomerStatusMapDAO;
import com.domain.Customer;
import com.domain.CustomerStatus;
import com.domain.CustomerStatusMap;

public class CustomerStatusMapService {

	CustomerStatusMapDAO customerStatusMapDAO;

	public CustomerStatusMapService() {
		customerStatusMapDAO = new CustomerStatusMapDAO();
	}

	public List<CustomerStatusMap> findAll() {
		return customerStatusMapDAO.findAll();
	}
	
	public void update(CustomerStatusMap customerStatusMap) {
		customerStatusMapDAO.update(customerStatusMap);
	}
	
	public CustomerStatusMap findByCustomer(Customer customer) {
		return customerStatusMapDAO.findByCustomer(customer);
	}
}