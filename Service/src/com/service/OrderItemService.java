package com.service;

import java.util.List;

import com.dao.domain.OrderItemDAO;
import com.domain.Order;
import com.domain.OrderItem;

public class OrderItemService {
	OrderItemDAO orderItemDAO;
	
	public OrderItemService() {
		orderItemDAO = new OrderItemDAO();
	}
	
	public List<OrderItem> findAllByOrder(Order order) {
		return orderItemDAO.findAllByOrder(order);
	}
}
