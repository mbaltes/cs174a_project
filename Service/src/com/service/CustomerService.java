package com.service;

import java.util.List;

import com.dao.domain.CustomerDAO;
import com.domain.Customer;

public class CustomerService {

	private CustomerDAO customerDAO;

	public CustomerService() {
		customerDAO = new CustomerDAO();
	}

	public List<Customer> findAll() {
		return customerDAO.findAll();
	}
	
	public Customer findByCustomerId(String customerId) {
		return customerDAO.findByCustomerId(customerId);
	}
}