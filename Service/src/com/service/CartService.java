package com.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.dao.domain.CartDAO;
import com.dao.domain.CustomerStatusMapDAO;
import com.dao.domain.OrderDAO;
import com.dao.domain.OrderItemDAO;
import com.domain.Cart;
import com.domain.Customer;
import com.domain.CustomerStatus;
import com.domain.Order;
import com.domain.OrderItem;

public class CartService {
	
	private OrderDAO orderDAO;
	private CartDAO cartDAO;
	private CustomerStatusMapDAO customerStatusMapDAO;
	private OrderItemDAO orderItemDAO;
	private CustomerStatusService customerStatusService;
	
	public CartService() {
		cartDAO = new CartDAO();
		customerStatusService = new CustomerStatusService();
		orderDAO = new OrderDAO();
		customerStatusMapDAO = new CustomerStatusMapDAO();
		orderItemDAO = new OrderItemDAO();
	}

	public void insert(Cart cart) {
		cartDAO.insert(cart);
	}

	public void delete(String customerId, String stockNumber) {
		cartDAO.delete(customerId, stockNumber);
	}

	public List<Cart> findAll(Customer customer) {
		return cartDAO.findAll(customer);
	}
	
	public void checkout(Customer customer) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		float totalPriceBeforeDiscount = 0.0f;
		
		//Get all the customers
		List<Cart> itemsInCart = findAll(customer);
		
		//Get the customers discount
		CustomerStatus customerStatus = customerStatusService.findByCustomer(customer);
		
		//Calculate the total
		for (Cart cart : itemsInCart)
		{
			totalPriceBeforeDiscount += cart.getPrice()*cart.getQuantity();
		}
		
		float totalPrice = totalPriceBeforeDiscount;
		
		// If the shopping cart total (excluding shipping and handling) exceeds
		// $100, the shipping and handling fee will be waived.
		if (totalPriceBeforeDiscount < customerStatus.getFreeShipping())
		{
			totalPrice *= (1+customerStatus.getShipRate()-customerStatus.getDiscount());
		}
		else
		{
			totalPrice *= (1-customerStatus.getDiscount());
		}
		
		//Insert the shopping cart
		Date date = new Date();
		
		Order order = new Order();
		order.setOrder(0, customer.getCustomerId(),
				customerStatus.getStatusId(), totalPriceBeforeDiscount,
				totalPrice, dateFormat.format(date), 0);
		
		// Insert the order
		orderDAO.insert(order);
		
		// Insert the order items
		
		List<OrderItem> associatedOrderItems = new ArrayList<OrderItem>();
		
		for (Cart cart : itemsInCart)
		{
			OrderItem orderItem = new OrderItem();
			orderItem.setOrderItem(0, order.getOrderId(), cart.getStockNumber(), cart.getQuantity());
			orderItemDAO.insert(orderItem);
			associatedOrderItems.add(orderItem);
		}
		
		// Delete the shopping cart
		cartDAO.deleteAll(customer);
		// Update the customer status
		customerStatusService.updateCustomerDiscountStatus(customer);
		
		System.out.println("Successfully checked out! Here are your order details...");
		Order.printHeader();
		order.printRowDisplay();
		System.out.println();
		System.out.println("The items that were bought...");
		associatedOrderItems.get(0).printHeader();
		
		for (OrderItem orderItem : associatedOrderItems)
		{
			orderItem.printRowDisplay();
		}
	}
	
	public Cart findByStockNumber(String stockNumber) {
		return cartDAO.findByStockNumber(stockNumber); 
	}
}