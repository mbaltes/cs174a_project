package com.service;

import java.util.List;

import com.dao.domain.CustomerDAO;
import com.dao.domain.CustomerStatusDAO;
import com.dao.domain.CustomerStatusMapDAO;
import com.dao.domain.OrderDAO;
import com.domain.Customer;
import com.domain.CustomerStatus;
import com.domain.CustomerStatusMap;
import com.domain.Order;

public class CustomerStatusService {

	CustomerStatusDAO customerStatusDAO;
	CustomerStatusMapDAO customerStatusMapDAO;
	OrderDAO orderDAO;
	CustomerDAO customerDAO;

	public CustomerStatusService() {
		customerStatusDAO = new CustomerStatusDAO();
		customerStatusMapDAO = new CustomerStatusMapDAO();
		orderDAO = new OrderDAO();
		customerDAO = new CustomerDAO();
	}

	public void update(CustomerStatus customerStatus) {
		customerStatusDAO.update(customerStatus);
	}
	
	public CustomerStatus findCustomerStatus(Customer customer) {
		return customerStatusDAO.findByCustomer(customer);
	}
	
	public CustomerStatus findByCustomer(Customer customer) {
		return customerStatusDAO.findByCustomer(customer);
	}
	
	public List<CustomerStatus> findAll() {
		return customerStatusDAO.findAll();
	}

	/**
	 * Give the customer a better status if they have passed the threshold of
	 * spending.
	 * 
	 * @param customer
	 * @return
	 */
	public void updateCustomerDiscountStatus(Customer customer) {
	
		//Given the customer status, lets find if they passed the next threshold.
		CustomerStatus customerStatus = findCustomerStatus(customer);
		int oldCustomerStatusId = customerStatus.getStatusId();
		
		//Get the available customer statuses
		List<CustomerStatus> customerStatuses = customerStatusDAO.findAll();
		
		//Get the total amount spent
		List<Order> orders = orderDAO.findLastThreeByCustomer(customer);
		
		//Keep track of the total amount spent
		float totalAmountSpent = 0.0f;
		
		for (Order order : orders)
			totalAmountSpent += order.getTotal();
		
		//Reset them back to green if they are not above the minimum
		if (customerStatus.getMinimum() > totalAmountSpent)
		{
			customerStatus.setStatusId(3);
		}
		
		for (CustomerStatus cs : customerStatuses)
		{
			//If the customer status is not the same and they have passed the threshold, upgradeeee!
			if (cs.getStatusId() != customerStatus.getStatusId()
					&& cs.getMinimum() > customerStatus.getMinimum()
					&& totalAmountSpent > cs.getMinimum()) {
				customerStatus = cs;
			}
		}
		
		//Check if it is a new customer status
		if (oldCustomerStatusId != customerStatus.getStatusId()) {
			CustomerStatusMap csm = customerStatusMapDAO.findByCustomer(customer);
			csm.setStatusId(customerStatus.getStatusId());
			
			//Update
			customerStatusMapDAO.update(csm);
		}
	}
	
	public void updateAllCustomerStatusDiscountStatus() {
		List<Customer> customers = customerDAO.findAll();
		
		for (Customer customer : customers) {
			updateCustomerDiscountStatus(customer);
		}
	}
}