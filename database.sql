CREATE TABLE Catalog(  stockNumber CHAR(7),
category VARCHAR2(128) NOT NULL,
manufacturer VARCHAR2(128) NOT NULL,
model VARCHAR2(128) NOT NULL,
warranty INTEGER NOT NULL,
price REAL NOT NULL,
PRIMARY KEY (stockNumber)
);

CREATE TABLE Descriptions(did INTEGER,
stockNumber CHAR(7),
attribute VARCHAR2(128) NOT NULL,
value VARCHAR2(2048) NOT NULL,
PRIMARY KEY (did),
FOREIGN KEY (stockNumber) REFERENCES Catalog ON DELETE CASCADE
);

CREATE TABLE Accessories( aid INTEGER,
parent CHAR(7),
child CHAR(7),
PRIMARY KEY (aid),
FOREIGN KEY (parent) REFERENCES Catalog ON DELETE CASCADE,
FOREIGN KEY (child) REFERENCES Catalog ON DELETE CASCADE
);

CREATE TABLE Customers( customerId VARCHAR2(128),
password VARCHAR2(128) NOT NULL,
name VARCHAR2(128) NOT NULL,
email VARCHAR2(128) NOT NULL,
address VARCHAR2(128) NOT NULL,
manager NUMBER(1) NOT NULL,
PRIMARY KEY (customerId)
);

CREATE TABLE CustomerStatus( statusId INTEGER,
name VARCHAR2(128) NOT NULL,
discount REAL NOT NULL,
shipRate REAL NOT NULL,
freeShipping REAL NOT NULL,
minimum REAL NOT NULL,
PRIMARY KEY (statusId)
);

CREATE TABLE CustomerStatusMap( mapId INTEGER,
customerId VARCHAR2(128),
statusId INTEGER,
PRIMARY KEY (mapId),
FOREIGN KEY(customerId) REFERENCES Customers,
FOREIGN KEY(statusId) REFERENCES CustomerStatus,
UNIQUE(customerId)
);

CREATE TABLE Cart( cid INTEGER,
customerId VARCHAR2(128),
stockNumber CHAR(7),
quantity INTEGER NOT NULL,
PRIMARY KEY (cid),
FOREIGN KEY (customerId) REFERENCES Customers,
FOREIGN KEY (stockNumber) REFERENCES Catalog
);

CREATE TABLE Orders( orderId INTEGER,
customerId VARCHAR2(128),
statusId INTEGER,
subtotal REAL NOT NULL,
total REAL NOT NULL,
time VARCHAR2(128) NOT NULL,
processed INTEGER NOT NULL,
PRIMARY KEY (orderId),
FOREIGN KEY (customerId) REFERENCES Customers,
FOREIGN KEY (statusId) REFERENCES CustomerStatus
);

CREATE TABLE OrderItems( id INTEGER,
orderId INTEGER,
stockNumber CHAR(7),
quantity INTEGER NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (orderId) REFERENCES Orders,
FOREIGN KEY (stockNumber) REFERENCES Catalog
);

CREATE TABLE Inventory( stockNumber CHAR(7),
manufacturer VARCHAR2(128) NOT NULL,
model VARCHAR2(128) NOT NULL,
quantity INTEGER NOT NULL,
minimum INTEGER NOT NULL,
maximum INTEGER NOT NULL, 
location VARCHAR2(128),
replenishment INTEGER NOT NULL,
PRIMARY KEY (stockNumber)
);

CREATE TABLE ShippingNotices( noticeId INTEGER,
shippingCompany VARCHAR2(128) NOT NULL,
time VARCHAR(128) NOT NULL,
received NUMBER(1) NOT NULL,
PRIMARY KEY (noticeId)
);

CREATE TABLE ShippingItems( itemId INTEGER,
noticeId INTEGER,
manufacturer VARCHAR2(128),
model VARCHAR2(128),
quantity INTEGER,
PRIMARY KEY (itemId),
FOREIGN KEY (noticeId) REFERENCES ShippingNotices
);

CREATE TABLE ReplenishmentOrders( orderId INTEGER,
manufacturer VARCHAR2(128),
time VARCHAR2(128),
received NUMBER(1) NOT NULL,
PRIMARY KEY (orderId)
);


CREATE TABLE ReplenishmentItems( itemId INTEGER,
orderId INTEGER,
manufacturer VARCHAR2(128),
model VARCHAR2(128),
quantity INTEGER,
PRIMARY KEY (itemId),
FOREIGN KEY (orderId) REFERENCES ReplenishmentOrders
);