INSERT INTO Catalog VALUES ('SN00101', 'Laptop', 'HP', '6111', 12, 1630.0);
INSERT INTO Catalog VALUES ('SN00201', 'Desktop', 'Dell', '420', 12, 239.0);
INSERT INTO Catalog VALUES ('SN00202', 'Desktop', 'Emachine', '3958', 12, 369.99);
INSERT INTO Catalog VALUES ('SN00301', 'Monitor', 'Envision', '720', 36, 69.99);
INSERT INTO Catalog VALUES ('SN00302', 'Monitor', 'Samsung', '712', 36, 239.0);
INSERT INTO Catalog VALUES ('SN00401', 'Software', 'Symantec', '2005', 60,19.99);
INSERT INTO Catalog VALUES ('SN00402', 'Software', 'Mcafee', '2005', 60, 19.99);
INSERT INTO Catalog VALUES ('SN00501', 'Printer', 'HP', '1320', 12, 299.99);
INSERT INTO Catalog VALUES ('SN00601', 'Camera', 'HP', '435', 3, 119.99);
INSERT INTO Catalog VALUES ('SN00602', 'Camera', 'Canon', '738', 1, 329.99);

INSERT INTO Descriptions VALUES(1,'SN00101', 'Processor speed', '3.33GHz');
INSERT INTO Descriptions VALUES(2,'SN00101', 'Ram size', '512MB');
INSERT INTO Descriptions VALUES(3,'SN00101', 'Hard disk size', '100GB');
INSERT INTO Descriptions VALUES(4,'SN00101', 'Display size', '17"');
INSERT INTO Descriptions VALUES(5,'SN00201', 'Processor speed', '2.53GHz');
INSERT INTO Descriptions VALUES(6,'SN00201', 'Ram size', '512MB');
INSERT INTO Descriptions VALUES(7,'SN00201', 'Hard disk size', '80GB');
INSERT INTO Descriptions VALUES(8,'SN00201', 'OS', 'none');
INSERT INTO Descriptions VALUES(9,'SN00202', 'Processor speed', '2.9GHz');
INSERT INTO Descriptions VALUES(10,'SN00202', 'Ram size', '512MB');
INSERT INTO Descriptions VALUES(11,'SN00202', 'Hard disk size', '80GB');
INSERT INTO Descriptions VALUES(12,'SN00301', 'Size', '17"');
INSERT INTO Descriptions VALUES(13,'SN00301', 'Weight', '25 lb.');
INSERT INTO Descriptions VALUES(14,'SN00302', 'Size', '17"');
INSERT INTO Descriptions VALUES(15,'SN00302', 'Weight', '9.6 lb.');
INSERT INTO Descriptions VALUES(16,'SN00401', 'Required disk size', '128MB');
INSERT INTO Descriptions VALUES(17,'SN00401', 'Required RAM size', '64MB');
INSERT INTO Descriptions VALUES(18,'SN00402', 'Required disk size', '128MB');
INSERT INTO Descriptions VALUES(19,'SN00402', 'Required RAM size', '64MB');
INSERT INTO Descriptions VALUES(20,'SN00501', 'Resolution', '1200 dpi');
INSERT INTO Descriptions VALUES(21,'SN00501', 'Sheet capacity', '500');
INSERT INTO Descriptions VALUES(22,'SN00501', 'Weight', '24.7 lb.');
INSERT INTO Descriptions VALUES(23,'SN00601', 'Resolution', '3.1 Mp');
INSERT INTO Descriptions VALUES(24,'SN00601', 'Max zoom', '5x');
INSERT INTO Descriptions VALUES(25,'SN00601', 'Weight', '0.4 lb.');
INSERT INTO Descriptions VALUES(26,'SN00602', 'Resolution', '3.1 Mp');
INSERT INTO Descriptions VALUES(27,'SN00602', 'Max zoom', '5x');
INSERT INTO Descriptions VALUES(28,'SN00602', 'Weight', '0.4 lb.');

INSERT INTO Accessories VALUES(1, 'SN00201','SN00301' );
INSERT INTO Accessories VALUES(2, 'SN00201','SN00302' );
INSERT INTO Accessories VALUES(3, 'SN00201','SN00401' );
INSERT INTO Accessories VALUES(4, 'SN00201','SN00402' );
INSERT INTO Accessories VALUES(5, 'SN00201','SN00501' );
INSERT INTO Accessories VALUES(6, 'SN00201','SN00601' );
INSERT INTO Accessories VALUES(7, 'SN00201','SN00602' );
INSERT INTO Accessories VALUES(8, 'SN00202','SN00301' );
INSERT INTO Accessories VALUES(9, 'SN00202','SN00302' );
INSERT INTO Accessories VALUES(10, 'SN00202','SN00401' );
INSERT INTO Accessories VALUES(11, 'SN00202','SN00402' );
INSERT INTO Accessories VALUES(12, 'SN00202','SN00501' );
INSERT INTO Accessories VALUES(13, 'SN00202','SN00601' );
INSERT INTO Accessories VALUES(14, 'SN00202','SN00602' );

INSERT INTO Inventory VALUES ('SN00101', 'HP', '6111', 2, 1, 10, 'A9', 0);
INSERT INTO Inventory VALUES ('SN00201', 'Dell', '420', 3, 2, 15, 'A7', 0);
INSERT INTO Inventory VALUES ('SN00202', 'Emachine', '3958', 4, 2, 8, 'B52', 0);
INSERT INTO Inventory VALUES ('SN00301', 'Envision', '720', 4, 3, 6, 'C27', 0);
INSERT INTO Inventory VALUES ('SN00302', 'Samsung', '712', 4,3,6, 'C13', 0);
INSERT INTO Inventory VALUES ('SN00401', 'Symantec', '2005', 7, 5, 9, 'D27', 0);
INSERT INTO Inventory VALUES ('SN00402', 'Mcafee', '2005', 7,5,9, 'D1', 0);
INSERT INTO Inventory VALUES ('SN00501', 'HP', '1320', 3,2,5,'E7',0);
INSERT INTO Inventory VALUES ('SN00601', 'HP', '435', 3,2,9,'F9',0);
INSERT INTO Inventory VALUES ('SN00602', 'Canon', '738', 3,2,5,'F3',0);

###CUSTOM DO NOT INCLUDE THESE IN DEMO
INSERT INTO Inventory VALUES ('SN00901', 'HP', 'c1', 2, 5, 10, 'A21', 2);
INSERT INTO Inventory VALUES ('SN00902', 'HP', 'c2', 2, 5, 10, 'A22', 1);
INSERT INTO Inventory VALUES ('SN00903', 'HP', 'c3', 2, 5, 10, 'A23', 0);
###ENDCUSTOM DO NOT INCLUDE THESE IN DEMO

select stockNumber, quantity, minimum, maximum, replenishment FROM Inventory;

truncate table ReplenishmentItems;
delete from ReplenishmentOrders where orderId < 10;
truncate table ShippingItems;
delete from ShippingNotices where noticeId < 100;

truncate table Inventory;
INSERT INTO Inventory VALUES ('SN00101', 'HP', '6111', 2, 1, 10, 'A9', 0);
INSERT INTO Inventory VALUES ('SN00201', 'Dell', '420', 3, 2, 15, 'A7', 0);
INSERT INTO Inventory VALUES ('SN00202', 'Emachine', '3958', 4, 2, 8, 'B52', 0);
INSERT INTO Inventory VALUES ('SN00301', 'Envision', '720', 4, 3, 6, 'C27', 0);
INSERT INTO Inventory VALUES ('SN00302', 'Samsung', '712', 4,3,6, 'C13', 0);
INSERT INTO Inventory VALUES ('SN00401', 'Symantec', '2005', 7, 5, 9, 'D27', 0);
INSERT INTO Inventory VALUES ('SN00402', 'Mcafee', '2005', 7,5,9, 'D1', 0);
INSERT INTO Inventory VALUES ('SN00501', 'HP', '1320', 3,2,5,'E7',0);
INSERT INTO Inventory VALUES ('SN00601', 'HP', '435', 3,2,9,'F9',0);
INSERT INTO Inventory VALUES ('SN00602', 'Canon', '738', 3,2,5,'F3',0);
commit;




