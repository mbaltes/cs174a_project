package eDepot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import com.domain.Inventory;
import com.domain.Order;
import com.domain.ShippingItem;
import com.domain.ShippingNotice;
import com.service.InventoryService;
import com.service.OrderItemService;
import com.service.OrderService;

public class Main {
	
	private static InventoryService inventoryService;
	private static OrderService orderService;
	private static OrderItemService orderItemService;
	
	public static void main(String[] args) throws IOException {
		inventoryService = new InventoryService();
		orderService = new OrderService();
		orderItemService = new OrderItemService();
		System.out.println("|---------------------------------|");
		System.out.println("|          eDepot Interface       |");
		System.out.println("|---------------------------------|");
		System.out.println("  q - Look up quantity of an item by Stock Number.");
		System.out.println("  n - Enter shipping notice information into the system.");
		System.out.println("  r - Receive a shipment with a prior shipping notice.");
		System.out.println("  f - Fill an unprocessed order.");
		System.out.println("  e - Exit the program.");
		
		InputStreamReader sr =new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(sr);
		
		String input = "";
		do {
			System.out.print("\n\nChoice: ");
			input = br.readLine();
			if(input.equals("q"))
			{
				lookupQuantity(br);
			}
			else if(input.equals("n")) {
				receiveShippingNotice(br);
			}
			else if(input.equals("r"))
			{
				receiveShipmentInterface(br);
			}
			else if(input.equals("f"))
			{
				fillOrderInterface(br);
			}
		} while( !input.equals("e"));
		
	}

	public static void lookupQuantity(BufferedReader br) throws IOException {
		System.out.println("Enter stock number: ");
		String input = br.readLine();
		Inventory item = inventoryService.findByStockNumber(input);
		if(item == null) {
			System.out.println("No item found with that stock number.\n");
		} else {
			System.out.printf("%-15s %-15s %-15s\n", "Stock Number", "Quantity", "Location");
			System.out.println("------------------------------------------");
			System.out.printf("%-15s %-15s %-15s\n", item.getStockNumber(), item.getQuantity(), item.getLocation());
		}
	}
	
	//interface for shipping notice entry.
	public static void receiveShippingNotice(BufferedReader br) throws IOException {
		System.out.print("Shipping company: ");
		String shipper = br.readLine();
		System.out.print("Date yyyy/MM/dd: ");
		String date = br.readLine();
		System.out.print("Number of different items: ");
		Integer count = Integer.parseInt(br.readLine());
		
		
		ShippingNotice notice = new ShippingNotice();
		
		//give this notice a primary key
		notice.setNoticeId(inventoryService.generateShippingNoticeId());
		
		//set shipping company
		notice.setShippingCompany(shipper);
		
		//set date
		notice.setTime(date);
		
		//set received to false
		notice.setReceived(0);
		
		LinkedList<ShippingItem> items = new LinkedList<ShippingItem>();
		
		for(int x = 0; x < count; x++) {
			//first, create new shipping notice object.
			ShippingItem current = new ShippingItem();
			
			//give shipping notice object a primary key.
			final int firstItemId = inventoryService.generateShippingItemId();
			//we need a different pk with each iteration so this will offset from the initial.
			current.setItemId(firstItemId + x);
			
			//next, we attach this item to the corresponding shipping notice object
			current.setNoticeId(notice.getNoticeId());
			
			//prompt user for manufacturer
			System.out.print("\nManufacturer: ");
			String manufacturer = br.readLine();
			current.setManufacturer(manufacturer);
			
			//prompt user for model
			System.out.print("Model: ");
			String model = br.readLine();
			current.setModel(model);
			
			//prompt user for quantity
			System.out.print("Quantity: ");
			Integer quantity = Integer.parseInt(br.readLine());
			current.setQuantity(quantity);
			//TODO: validate inputs. don't allow negative quantity.
			
			//add to list of items to be persisted.
			items.add(current);
		}
		
		//persist shipping notice
		inventoryService.persistShippingNotice(notice, items);
		
		//udpate replenishment information.
		for(ShippingItem item : items)
		{
			Inventory inv = inventoryService.findByManufacturerModel(item.getManufacturer(), item.getModel());
			if(inv == null) {
				//this is a new item.
				inv = new Inventory();
				//generate a new stock number
				inv.setStockNumber(inventoryService.generateNewStockNumber());
				//fill in the information based on current things known.
				inv.setManufacturer(item.getManufacturer());
				inv.setModel(item.getModel());
			    inv.setQuantity(item.getQuantity());
			    inv.setReplenishment(0);
			    inv.setLocation(inventoryService.generateNewLocation());
				
				//prompt user to enter information about this new item.
				System.out.println("\n\n*** A new item has been found in the order ***");
				System.out.println("Stock Number: " + inv.getStockNumber());
				System.out.println("Manufacturer: " + inv.getManufacturer());
				System.out.println("Model: " + inv.getModel());
				System.out.println("Quantity: " + inv.getQuantity());
				System.out.println("Location: " + inv.getLocation());
				
				//read in max.
				System.out.println("Enter Max: ");
				inv.setMax(Integer.parseInt(br.readLine()));
				
				//read in min.
				System.out.println("Enter Min: ");
				inv.setMin(Integer.parseInt(br.readLine()));
				
				inventoryService.persistInventory(inv);
				
			} else {
				inv.setReplenishment(inv.getReplenishment() + item.getQuantity());
				inventoryService.updateInventoryQuantityReplenishments(inv);
			}
		}
	}
	
	public static void receiveShipmentInterface(BufferedReader br) {
		//show user list of all un-received Shipping Notices
		//prompt user to choose one by id
		//mark that as receieved
		//process all entries from that.
		//TODO: don't do autocommit for this probably.
		List<ShippingNotice> notices = inventoryService.findUnreceivedShippingNotices();
		
		//nothing to process, return with a message.
		if(notices == null) {
			System.out.println("There are no shipping notices which have not been received.");
			return;
		}
		
		//there are things to process, let's process.
		System.out.println("Unreceived shipping notices:");
		System.out.printf("%-15s %-15s %-15s\n", "Notice ID", "Shipping Company", "Date");
		System.out.println("--------------------------------------------------------------");
		
		//output shipping notices to user.
		for(ShippingNotice notice : notices) {
			System.out.printf("%-15s %-15s %-15s\n", notice.getNoticeId(), notice.getShippingCompany(), notice.getTime());
		}
		
		//prompt user to choose
		Integer choice = null;
		System.out.print("Enter notice id: ");
		try {
			choice = Integer.parseInt(br.readLine());
			
			//receive the shipment notice.
			ShippingNotice notice = inventoryService.findByNoticeId(choice);
			List<Inventory> results = inventoryService.receiveShipment(notice);
			
			Inventory.printHeader();
			for(Inventory inv : results) {
				inv.printRowDisplay();
			}
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			System.out.println("Invalid input.");
			return;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}
	
	public static void fillOrderInterface(BufferedReader br) throws IOException {
		List<Order> orders = orderService.findAllUnprocessed();
		
		//check if there are no unfilled orders
		if(orders == null) {
			System.out.println("There are no orders to be filled.\n");
			return;
		}
		
		//prompt user with choices.
		System.out.printf("%-15s %-15s %-15s %-15s\n", "Order ID", "Customer ID", "Total", "Date");
		System.out.println("---------------------------------------------------------------------------");
		for(Order order : orders) {
			System.out.printf("%-15s %-15s %-15s %-15s\n", order.getOrderId(), order.getCustomerId(), order.getTotal(), order.getTime());
		}
		System.out.print("\nEnter an order id to fill, or type all to fill all orders\nOrder: ");
		String input = br.readLine();
		if(input.equals("all")) {
			inventoryService.fillOrders(orders);
		} else {
			try {
				int orderId = Integer.parseInt(input);
				Order order = orderService.findByOrderId(orderId);
				if(order.getProcessed() == 1) {
					System.out.println("That order id has already been filled.\n");
					return;
				}
				
				if(!inventoryService.fillOrder(order)) {
					System.out.println("Order number " + order.getOrderId() + " could not be filled due to insufficient quantity in warehouse.");
					System.out.println("However, automatic replenishments may have been submitted");
					System.out.println("Order more items and/or receive a shipment to fill this order.\n");
				} else {
					System.out.println("Order number " + order.getOrderId() + " was filled successfully.\n");
				}
			} catch (NumberFormatException ex) {
				System.out.println("Invalid input. Not a number or all.\n");
			}
		}
		
		
	}
}
